function SongListUI(){

	this._scrollY;
	this.menuItemHeight;
	this.menuItems;
	
	this._obj;				// Createjs Container for this 'stage'
	
	this.init(){
		this._obj = new createjs.Container();
		this._obj.name = 'mainui';
		this._obj.alpha = 0;
		
		// If there is an old instance, remove from _stage, and add the new one
		var old = _stage.getChildByName(obj.name);
		if(!!old) _stage.removeChild(old);
		
		// Add the new UI
		_stage.addChild(this._obj);
		
		// Call the Create method
		this.create();
	}
	
	this.create = function(){
		// Create all CreateJS stuff here // basic menu images should already be loaded from start!
		
		// Fill the this._obj container with all the created elements
		
		// add this._obj to mainstage with alpha 0 ( invisible ) // fade in
		
		// this.onLoad(); after creation is successful
	}
	
	this.onLoad = function(){
		// What happens when this UI shows up? fade in? menu scrolls from side? etc. Fancy animations for Menu transitions
	}
	
	this.onLeave = function(nextStage){
		// Do specific shizzle for this UI item, when it should animate-out ( like scroll menu out of window )
		
		// Fadeout this._obj to alpha 0; and remove from Game._stage!
		
		// Next Stage is the UI that should be navigated to. in the Callback of the fadeouts and whatnot, nextStage.init(); should be called;
		
		// Next stage.init() should be called when the _loader is done :(
		
		// Maybe set currentStage to nextStage; and let the _loader call the init() on the currentStage, when it's 
	}

	this._update(){
		// Update gets called from MainController -> UIController -> SongListUI, so that specific update required elements can be rendered here
		// Update gets called on _current only!
	}
}