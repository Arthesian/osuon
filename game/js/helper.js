Helper = function(){
	
	// General functions
	
	this.osuXYtoGameXY = function(x, y){
		var GameX = 0.5 * (Game._stage.canvas.width - 700) + (x / 512) * 700;
		var GameY = 100 + (y / 384) * (Game._stage.canvas.height - 200);
		
		return {x: GameX, y: GameY};
	}

	this.sortByZ = function(a, b){
		if (a.zIndex < b.zIndex) return -1;
		if (a.zIndex > b.zIndex) return 1;
		return 0;
	}
	
	this.pythagoras = function(a ,b){
		return Math.sqrt(a*a + b*b);
	}
	
	this.removeTweenObject = function(tween){
		Game._stage.removeChild(tween._target);
	}
	
	this.drawSegment = function(target, x, y, r, aStart, aEnd, step) {
	
		if(!step) step = 10;
		// More efficient to work in radians
		var degreesPerRadian = Math.PI / 180;
		aStart *= degreesPerRadian;
		aEnd *= degreesPerRadian;
		step *= degreesPerRadian;
		
		// Draw the segment
		target.graphics.moveTo(x, y);
		for (var theta = aStart; theta < aEnd; theta += Math.min(step, aEnd - theta)) {
			target.graphics.lineTo(x + r * Math.cos(theta), y + r * Math.sin(theta));
		}
		target.graphics.lineTo(x + r * Math.cos(aEnd), y + r * Math.sin(aEnd));
		target.graphics.lineTo(x, y);
	};
	
	this.checkKey = function(event){
		
		// On Escape :: Show pause game if inGame
		if(event.keyCode == 27){
			if(Game._isPaused && Game._stage.children.indexOf(Game.UI.pause._obj) == -1){
				Game.resume();
			}else if(Game._stage.children.indexOf(Game.UI.pause._obj) == -1){
				Game.pause();
			}
		}
	}
}