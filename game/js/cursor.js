function Cursor(){
	
	// Properties
	
	this._obj;
	
	this._emitStars = false;

	this._starObj;
	
	// Methods
	
	this.init = function(){
		
		var _cursor;
		
		_cursor = new createjs.Container();
		_cursor.width = 76;
		_cursor.height = 76;
		_cursor.name = "cursor";
		
		_cursor.x = -76;
		
		var inner = new createjs.Bitmap(Game.loader.queue.getResult("cursor-mid"));
		var outer = new createjs.Bitmap(Game.loader.queue.getResult("cursor"));
			
		inner.regX = inner.regY = inner.image.width * 0.5;
		outer.regX = outer.regY = outer.image.width * 0.5;
			
		createjs.Tween.get(outer, {loop:true}).to({rotation: 360}, 4000);
			
		_cursor.addChild(inner, outer);
		_cursor.zIndex = 9999;
		
		var star = new createjs.Bitmap(Game.loader.queue.getResult("star2"));
		star.regX = star.image.width / 2;
		star.regY = star.image.height / 2;
		star.zIndex = 1;
		star.alpha = 0;
		
		// Check if the GameStage already has a 'cursor' child
		var oldCur = Game._stage.getChildByName('cursor');
		if(oldCur){
			Game._stage.removeChild(oldCur);
		}
		
		Game._stage.addChild(_cursor);
		
		this._obj = _cursor;
		this._starObj = star;
		
		this._obj.addChild(this._starObj);
		
		Game._cursor = this;
		
		$("#game_canvas").on('keypress', Game._cursor.doClick);
		$("#game_canvas").on('click', Game._cursor.doClick);
	}
	
	this.startEmit = function(){
		this._emitStars = true;
		this._emit();
	}
	
	this.stopEmit = function(){
		this._emitStars = false;
	}
	
	this._emit = function(){
		if(this._emitStars){
		
			var star = Game._cursor._starObj.clone();
			star.x = this._obj.x + (-20 + Math.random() * 40);
			star.y = this._obj.y + (-20 + Math.random() * 40);
			star.rotation = Math.random() * 360;
		
			var diffX = (-50 + Math.random() * 100);
			var diffY = 200 + Math.random() * 100;
		
			// Loop this function
			setTimeout(function(){
				Game._cursor._emit();
			}, 50);
			
			Game._stage.addChild(star);
			
			createjs.Tween.get(star).to({alpha: 1}, 100).to({alpha: 0}, 400);
			createjs.Tween.get(star).to({x: star.x + diffX, y: star.y + diffY, }, 500, createjs.Ease.cubicIn).call(Game.helper.removeTweenObject);
		}
	}
	
	this._burst = function(){
	
	}
	
	this.update = function(){
		this._obj.x = Game._stage.mouseX;
		this._obj.y = Game._stage.mouseY;
	}
	
	this.doClick = function (){
		createjs.Tween.get(Game._cursor._obj).to({scaleX:1.2, scaleY:1.2}, 50).to({scaleX:1, scaleY:1}, 100);
	}
}