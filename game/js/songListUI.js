function SongListUI(){

	this._scrollY = 0;
	
	this.menuHeight;
	this.menuItemHeight;
	this.menuItems;
	
	this._selectedItem;
	
	this._obj;				// Createjs Container for this 'stage'
	
	this.init = function(){
		var obj = new createjs.Container();
		obj.name = 'songlistui';
		obj.alpha = 0;
		obj.zIndex = 100;
		
		// If there is an old instance, remove from _stage, and add the new one
		var old = Game._stage.getChildByName(obj.name);
		if(!!old) Game._stage.removeChild(old);
		
		// Add the new UI
		this._obj = obj;
		
		Game._stage.addChild(this._obj);
	}
	
	this.create = function(data){
		// Create all CreateJS stuff here // basic menu images should already be loaded from start!
		var cont = new createjs.Container();
		cont.name = "songList";
		
		this.menuItems = [];
		this.menuItemHeight = 103;
		this.menuHeight = data.length * this.menuItemHeight + 60;
		
		// TODO: sort list by difficulty / artist / A-Z / Z-A / songname etc. etc.
		for(i in data){
			cont.addChild(this.createSongMenuItem(data[i], i));
		}
		
		cont.width = Game._stage.canvas.width;
		cont.height = Game._stage.canvas.height;
		cont.x = 1280; // offScreen ( default :: 700 )
		cont.y = 0;
		
		cont.zIndex = 100;
		
		Game.UI._songMenu = this;
		
		var old = this._obj.getChildByName(cont.name); 
		if(!!old){
			
			createjs.Tween.get(cont).to({x:700},1000, createjs.Ease.cubicInOut); // set X to 0 == slide-in effect
			this._obj.removeChild(old);
			
		}else {
			createjs.Tween.get(cont).to({x:700},1000, createjs.Ease.cubicInOut); // set X to 0 == slide-in effect
		}
		
		this._obj.addChild(cont);
		
		Game._stage.sortChildren(Game.helper.sortByZ);
	}
	
	this.createSongMenuItem = function(map, index){

		map.index = index; // save the total Index;
		
		var bg = new createjs.Bitmap(Game.loader.queue.getResult("menu-button-background"));
		bg.name = "menu-item-bg";
		// Create an list item for every beatmapIndex
		var li = new createjs.Container();
		
		li.x = 0;// Game._stage.canvas.width - bg.image.width + 50;
		li.y = bg.image.height * index + 50;
		li.index = index;
		li.height = bg.image.height;
		li.width = bg.image.width;
		li.data = map;
		li.alpha = 0.8;
		
		var txt = new createjs.Text(map.data.artist + '\n' + map.data.song, "20px Arial", "black");
		txt.x = 20;
		txt.y = 15;
		
		var diffTxt = new createjs.Text(map.metadata.Version, "16px Arial", "black");
		diffTxt.x = 20;
		diffTxt.y = 70;
		
		li.addChild(bg, txt, diffTxt);
		
		li.addEventListener('rollout', function(event){
			createjs.Tween.removeTweens(event.target);
			createjs.Tween.get(event.currentTarget).to({alpha: 0.8}, 500);		
		});
		li.addEventListener('rollover', function(event){
			createjs.Tween.removeTweens(event.target);
			createjs.Tween.get(event.currentTarget).to({alpha: 1}, 500);
		});
		
		li.addEventListener('click', this.songSelected);
		
		_menuHelper.songMenuItemCount++;
		
		return li;
	}
	
	this.load = function(){
		
		//set current to this UI
		Game.UI._current = Game.UI.songList;
		
		Game.UI.songList.init();
		
		if($("#searchBox").val()){
			Game.UI.songList.search($("#searchBox").val());
		}else{
			Game.UI.songList.create(Game.beatmaps);
		}
		
		Game.UI.songList.onLoad();
		
		Game._stage.enableMouseOver(15);
		
		Game.mediaplayer.play();
		
		$("#game_topbar").animate({top: 0, opacity: 1}, 1000);
		
		Game.UI.startEmittingStars();
		
		Game._stage.sortChildren(Game.helper.sortByZ);
	}
	
	this.onLoad = function(){
		// What happens when this UI shows up? fade in? menu scrolls from side? etc. Fancy animations for Menu transitions
		createjs.Tween.get(this._obj).to({alpha:1},1000);
	}
	
	this.songSelected = function(event){
		
		// Let the loader handle the callback for setBackground in UIController, and Play Music in MusicPlayer ( if preview is present )
		Game._beatmap = event.target.parent.data;
		
		if(Game.UI.songList._selectedItem && Game.UI.songList._selectedItem == event.target.parent){
			
			Game.UI.songList._selectedItem.getChildByName("menu-item-bg").uncache();

			Game.initializeGame(Game.UI.songList._selectedItem.data);
			
			Game.UI.songList.onLeave();
	
		}else{
		
			if(!!Game.UI.songList._selectedItem && Game.UI.songList._selectedItem != event.target.parent)
				Game.UI.songList._selectedItem.getChildByName("menu-item-bg").uncache();
		
			Game.loader._loadPreview(event.target.parent.data);
				
		}
		
		Game.UI.songList._selectedItem = event.target.parent;
	}

	this.onLeave = function(nextStage){
		
		Game.UI.setBackground(true);
		
		Game.mediaplayer.stop();
		
		var cont = Game.UI.songList._obj.getChildByName("songList"); 
		// Do specific shizzle for this UI item, when it should animate-out ( like scroll menu out of window )
		createjs.Tween.get(cont).to({x:1280},500, createjs.Ease.cubicInOut);
		// Fadeout this._obj to alpha 0; and remove from Game._stage!
		createjs.Tween.get(Game.UI._current._obj).to({alpha:0},500).call(Game.helper.removeTweenObject);
		// Next Stage is the UI that should be navigated to. in the Callback of the fadeouts and whatnot, nextStage.init(); should be called;
		$("#game_topbar").animate({top: -50, opacity: 0}, 500);
		// Next stage.init() should be called when the _loader is done :(
		Game.UI.stopEmittingStars();
		// Maybe set currentStage to nextStage; and let the _loader call the init() on the currentStage, when it's 
	}
	
	this.search = function(term){
		// Search the goddamn list. 
		var m = Game.UI._songMenu._obj.getChildByName("songList");
		
		var _searchData = [];
		this._scrollY = 0;
		
		if(!term){ this.create(Game.beatmaps); _searchData = null; return;}
		
		for(i = 0; i < Game.beatmaps.length; i++){
			if(Game.beatmaps[i].data.name.toLowerCase().trim().indexOf(term.toLowerCase()) > -1){
				_searchData.push(Game.beatmaps[i]);
			}
		}
		
		this.create(_searchData);
		
		// TODO :: not just search for artist/song name : but also search for terms like '>3 star' && '<9 AR' && '=5 OD'
	}
	
	this.onScroll = function(delta){
		this._scrollY += delta;
		
		var canvas = Game._stage.canvas;
	
		if(this.menuHeight < canvas.height) return;
		
		if(this._scrollY > 0) this._scrollY = 0;
		
		if(this._scrollY < -this.menuHeight + canvas.height ){
			this._scrollY = -this.menuHeight + canvas.height;
		}
		
		var m = Game.UI._songMenu._obj.getChildByName("songList");
		
		createjs.Tween.removeTweens(m);
		createjs.Tween.get(m).to({y: this._scrollY}, 1500, createjs.Ease.getBackOut(2.5));
	}
	
	this._update = function(){
		// Update gets called from MainController -> UIController -> SongListUI, so that specific update required elements can be rendered here
		// Update gets called on _current only!
		
		var m = Game.UI._songMenu._obj.getChildByName("songList");
		var midY = Game._stage.canvas.height * .5; 
		
		for(i in m.children){
			var child = m.children[i];
			
			var relY = m.y + child.y;
			
			if(relY < midY){
				child.x = (midY - relY) * (midY - relY) * .001;
			}else{
				child.x = (relY - midY) * (relY - midY) * .001;
			}
			
			if(child == Game.UI.songList._selectedItem){
				var bg = child.getChildByName("menu-item-bg");
				
				child.x -= 50;
				bg.filters = [ new createjs.ColorFilter(1,0.5,1,1, 255,100,0,0) ]
				
				bg.cache(0,0,700,100);
			}
		}
	}
}