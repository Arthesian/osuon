// JavaScript Document

_menuHelper = new MenuHelper();

function MenuHelper(){

	this._scrollY = 0;
	
	this._updateMenu = function(){
		if(!!Game._mainUI._songMenu){
			this._updateSongMenu();
		}
		
	}

	this._mainMenu;
	this.loadMainMenu = function(){
		// TODO: create main menu
	}
	
	this.unloadMainMenu = function(){
		// TODO: unload main menu
	}

	this.songMenuHeight = 0;
	this.songMenuItemCount = 0;
	this.loadSongMenu = function(data){
		
		var cont = new createjs.Container();
		cont.name = "songMenu";
		
		_menuHelper.songMenuItemCount = 0;
		
		// TODO: sort list by difficulty / artist / A-Z / Z-A / songname etc. etc.
		for(i in data){
			for(j in data[i].beatmaps){
				cont.addChild(this.getSongMenuItem(i, data, j));
			}
		}
		
		cont.width = Game._stage.canvas.width;
		cont.height = Game._stage.canvas.height;
		cont.x = 1280; // offScreen ( default :: 700 )
		cont.y = 0;
		
		this.unloadSongMenu();
		
		cont.zIndex = 100;
		
		_menuHelper.songMenuHeight = _menuHelper.songMenuItemCount * 103 + 60;
		
		Game._stage.addChild(cont);
		
		createjs.Tween.get(cont).to({x:700},1000, createjs.Ease.cubicInOut); // set X to 0 == slide-in effect
		
		Game._mainUI._songMenu = cont;
		
		Game._stage.sortChildren(sortByZ);
	}
	
	this.getSongMenuItem = function(songIndex, set, beatmapIndex){
		
		var song = set[songIndex];
		var map = song.beatmaps[beatmapIndex];
		map.data = song;
		map.index = beatmapIndex; // save the total Index;
		
		var bg = new createjs.Bitmap(Game._loadQueue.getResult("menu-button-background"));
		
		// Create an list item for every beatmapIndex
		var li = new createjs.Container();
		
		li.x = 0;// Game._stage.canvas.width - bg.image.width + 50;
		li.y = bg.image.height * this.songMenuItemCount + 50;
		li.index = songIndex + beatmapIndex;
		li.height = bg.image.height;
		li.width = bg.image.width;
		li.data = map;
		li.alpha = 0.7;
		
		var txt = new createjs.Text(song.artist + '\n' + song.song, "20px Arial", "black");
		txt.x = 20;
		txt.y = 15;
		
		var diffTxt = new createjs.Text(map.difficulty, "16px Arial", "black");
		diffTxt.x = 20;
		diffTxt.y = 70;
		
		li.addChild(bg, txt, diffTxt);
		
		li.addEventListener('rollout', function(event){		
			createjs.Tween.get(event.currentTarget).to({alpha: 0.7}, 500);		
		});
		li.addEventListener('rollover', function(event){
			createjs.Tween.get(event.currentTarget).to({alpha: 1}, 500);
		});
		
		li.addEventListener('click', this.songMenuItemSelected);
		
		_menuHelper.songMenuItemCount++;
		
		return li;
	}
	
	this.songMenuItemSelected = function(event){
		var beatmap = event.target.parent.data; 
		
		// FUCK INIT2()
		Game.startGame(beatmap);
	}
	
	this._updateSongMenu = function(){
		var m = Game._mainUI._songMenu;
		var midY = Game._stage.canvas.height * .5; 
		
		for(i in m.children){
			var child = m.children[i];
			
			var relY = m.y + child.y;
			
			if(relY < midY){
				child.x = (midY - relY) * (midY - relY) * .001;
			}else{
				child.x = (relY - midY) * (relY - midY) * .001;
			}
		}
	}
	
	this.searchSongMenu = function(term){
		Game._mainUI._songMenu.y = 0;

		var _searchData = [];
		this._scrollY = 0;

		_menuHelper.unloadSongMenu();
		
		if(!term){ _menuHelper.loadSongMenu(_data); _searchData = null; return;}
		
		for(i = 0; i < _data.length; i++){
			if(_data[i].name.toLowerCase().trim().indexOf(term.toLowerCase()) > -1){
				_searchData.push(_data[i]);
			}
		}
		_menuHelper.loadSongMenu(_searchData);
	}
	
	this.unloadSongMenu = function(){
		// Remove old menu if already in stage
		var oldSongMenu = Game._stage.getChildByName('songMenu');
		if(oldSongMenu){
			Game._stage.removeChild(oldSongMenu);
			this.songMenuItemCount = 0;
			this.songMenuHeight = 0;
			
			// TODO: create nice ''slide-out'' effect for menu
			Game._mainUI._songMenu = null;
			//createjs.Tween.get(Game._mainUI._songMenu).to({x:_menuItemWidth + 50},500, createjs.Ease.cubicInOut).call(removeTweenObject);
		}
	}
}

// GLOBAL EVENT BINDING

$(document).ready(function(){
    $("#game_canvas").on('mousewheel', function(event){      
            if(Game.UI.songList == Game.UI._current)
				Game.UI.songList.onScroll(-event.originalEvent.deltaY);
        }
    );
	
	$(document).ready(function () {
       $('#searchBox').keyup(function () { Game.UI.songList.search($('#searchBox').val()) });
   });
});