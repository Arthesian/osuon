Beatmap = function(){
	
	// Beatmap Properties
	this.general;
	this.metadata;
	this.difficulty;
	this.colours;
	this.events;
	this.hitobjectList;
	this.timingPoints;
	
	this._prevHitobject;
	this._currentHitobject;
	this._nextHitobject;
	
	// TODO :: fill these properties!
	this.stars;			// Calculated Difficulty based on AC, OD, LD, BPM, Length(?), number of hitobjects
	this.CS				// Circle Size
	this.AR; 			// ApproachRate
	this.OD;			// Overall Difficulty
	this.LD;			// LifeDrain
	this.BPM;			// Beats Per Minute ( if available? )
	
	this.reset = function(){
		this._prevHitobject = null;
		this._currentHitobject = this.hitobjectList[0];
		this._nextHitobject = this.hitobjectList[1];
	}
	
	// Beatmap Methods
	this.parseFromTxt = function(txt){

		var rows = txt.split("\r\n");
		
		for( var i = 0; i < rows.length; i++){
			
			if(rows[i].indexOf("[General]") > -1){
				this.general = this.parseGeneral(rows, i + 1,'General', 'Editor');
			}
			
			if(rows[i].indexOf("[Metadata]") > -1){
				this.metadata = this.parseGeneral(rows, i + 1,'Metadata', 'Difficulty');
			}
			
			if(rows[i].indexOf("[Difficulty]") > -1){
				this.difficulty = this.parseGeneral(rows, i + 1,'Difficulty', 'Events');
			}
			
			if(rows[i].indexOf("[Colours]") > -1){
				this.colours = this.parseGeneral(rows, i + 1,'Colours', 'HitObjects');
			}
			
			if(rows[i].indexOf("[Events]") > -1){
				this.background = this.getBackgroundName(rows, i + 1,'Events', 'TimingPoints');
			}
			
			if(rows[i].indexOf("[TimingPoints]") > -1){
				this.timingPoints = this.getTimingPoints(rows, i + 1,'TimingPoints', 'Colours');
			}
			
			if(rows[i].indexOf("[HitObjects]") > -1){
				this.hitobjectList = this.parseHitobjectList(rows, i + 1);
				break;
			}		
		}		

		this.reset();
		
		this.BPM = this.timingPoints[0].BPM;
		this.AR = 300 + (10 - parseInt((this.difficulty.ApproachRate) ? this.difficulty.ApproachRate: 7)) / 9 * 1200;
		this.CS = 25 + (10 - parseInt(this.difficulty.CircleSize)) / 9 * 125;
		// Return parsed SELF
		return this;
	}
	
	this.getTimingPoints = function(data, i, from, untill){
		
		var obj = [];
		
		while(!(data[i].indexOf("[") > -1)){
			if(data[i].length < 2) {i++; continue;}
			if(data[i].indexOf("//") > -1) {i++; continue;}
		
			var dump = data[i].split(',');
			
			var tmp = {};
			
			tmp.time = parseFloat(dump[0]);
			tmp.beatTime = parseFloat(dump[1]);
			tmp.BPM = 60000 / tmp.beatTime;
			tmp.cursorStarActive = 0;
			tmp.hitStarBurstActive = 0;
			
			obj.push(tmp);
			
			i++;
		}
		
		console.log('TimingPoints found for ' + this.general['']);
		
		return obj;
	}

	this.getBackgroundName = function(data, i ,from, untill){
		var bg;
		
		while(!(data[i].indexOf("[" + untill + "]") > -1)){
			if(data[i].length < 2) {i++; continue;}
		
			if(data[i].indexOf("//") > -1) {i++; continue;}
			
			if(data[i].charAt(0) == 0){
				
				var tmp = data[i].split(',');
				
				bg = tmp[2].replace(/"/g, "");
				break;
			}		
			i++;
		}		
		return bg;
	}
	
	this.parseGeneral = function(data, i, from, untill){
		var obj = [];

		while(!(data[i].indexOf("[" + untill + "]") > -1)){
			if(data[i].length < 2) {i++; continue;}
		
			var keyVal = data[i].split(':');
			
			var value = (from == 'Metadata' || from == 'Difficulty') ? keyVal[1] : keyVal[1].substring(1);
		
			if(from == 'Difficulty'){
				obj[keyVal[0]] = parseFloat(value);
			}else{
				obj[keyVal[0]] = value;
			}

			i++;
		}
		
		if(from == "General"){
			obj.AudioLeadIn = parseFloat(obj['AudioLeadIn']);
			obj.PreviewTime = parseFloat(obj['PreviewTime']);
			obj.StackLeniency = parseFloat(obj['StackLeniency']);
		}
		
		return obj;
	}
	
	this.parseHitobjectList = function(data, i){
		var obj = [];
		var count = 0;
		
		for(i; i < data.length; i++ ){
			var ho = new HitObject();
			ho.parseData(data[i]);
			ho.index = parseInt(count);
			obj.push(ho);
			
			count++;
		}		
		return obj;
	}
}