PauseUI = function(){
	this._obj;
	
	this.init = function(){
		this.create();
	}
	
	this.create = function(){
		var menu = new createjs.Container();
		menu.alpha = 0;
		menu.zIndex = 100;
		menu.name = "pausemenu"
		
		var bg = new createjs.Shape();
		bg.graphics.f("rgba(0,0,0,0.7").drawRect(0,0,Game._stage.canvas.width,Game._stage.canvas.height);
		
		var midY = (Game._stage.canvas.height - 411) / 2;
		var midX = (Game._stage.canvas.width - 411) / 2; 
		
		var spacing = 150;
		
		var btn_back = new createjs.Bitmap(Game.loader.queue.getResult("btn-back-to-menu"));
		btn_back.regX = btn_back.image.width / 2;
		btn_back.regY = btn_back.image.height / 2;
		btn_back.x = midX + btn_back.regX;
		btn_back.y = midY + btn_back.regY + spacing * 2;
		btn_back.name = "back";
		
		var btn_continue = new createjs.Bitmap(Game.loader.queue.getResult("btn-continue"));
		btn_continue.regX = btn_continue.image.width / 2;
		btn_continue.regY = btn_continue.image.height / 2;
		btn_continue.x = midX + btn_continue.regX;
		btn_continue.y = midY + btn_continue.regY;
		btn_continue.name = "resume";
		
		var btn_retry = new createjs.Bitmap(Game.loader.queue.getResult("btn-retry"));
		btn_retry.regX = btn_retry.image.width / 2;
		btn_retry.regY = btn_retry.image.height / 2;
		btn_retry.x = midX + btn_retry.regX;
		btn_retry.y = midY + btn_retry.regY + spacing;
		btn_retry.name = "retry";
		
		btn_back.addEventListener("click", Game.UI.pause.onLeave);
		btn_continue.addEventListener("click", Game.UI.pause.onLeave);
		btn_retry.addEventListener("click", Game.UI.pause.onLeave);
		
		btn_back.addEventListener('rollout', function(event){
			var t = createjs.Tween.get(event.currentTarget, {override: true}).to({scaleX: 1, scaleY: 1}, 100);
			t.ignoreGlobalPause = true;
		});
		btn_back.addEventListener('rollover', function(event){
			var t = createjs.Tween.get(event.currentTarget, {override: true}).to({scaleX: 1.1, scaleY:1.1}, 100);
			t.ignoreGlobalPause = true;
		});
		
		btn_continue.addEventListener('rollout', function(event){
			var t = createjs.Tween.get(event.currentTarget, {override: true}).to({scaleX: 1, scaleY: 1}, 100);
			t.ignoreGlobalPause = true;			
		});
		btn_continue.addEventListener('rollover', function(event){
			var t = createjs.Tween.get(event.currentTarget, {override: true}).to({scaleX: 1.1, scaleY:1.1}, 100);
			t.ignoreGlobalPause = true;
		});
		
		btn_retry.addEventListener('rollout', function(event){
			var t = createjs.Tween.get(event.currentTarget, {override: true}).to({scaleX: 1, scaleY: 1}, 100);
			t.ignoreGlobalPause = true;			
		});
		btn_retry.addEventListener('rollover', function(event){
			var t = createjs.Tween.get(event.currentTarget, {override: true}).to({scaleX: 1.1, scaleY:1.1}, 100);
			t.ignoreGlobalPause = true;
		});
		
		menu.addChild(bg, btn_back, btn_retry, btn_continue);
		
		this._obj = menu;
	}
	
	this.load = function(){
		if(!Game.UI.pause._obj){
			Game.UI.pause.init();
		}
		
		Game._stage.addChild(Game.UI.pause._obj);
		
		Game._stage.sortChildren(Game.helper.sortByZ);
		
		Game.UI.pause.onLoad();
	}
	
	this.onLoad = function(){
		
		Game._stage.enableMouseOver(15);
	
		var t = createjs.Tween.get(Game.UI.pause._obj).to({alpha:1},200);
		t.ignoreGlobalPause = true;
	}
	
	this.onLeave = function(arg){
	
		var action = "";
	
		if(!!arg && !!arg.target) action = arg.target.name;
	
		Game._stage.enableMouseOver(0);
	
		var Tween = createjs.Tween.get(Game.UI.pause._obj).to({alpha:0},200);
		Tween.ignoreGlobalPause  = true;
	
		if(action == "back"){
		
			Tween.call(function(){
				
				Game.UI.pause._left;
				
				Game.UI.load('songList');
			});
		
		}else if(action == "continue"){
		
			Tween.call(Game.UI.pause._left);
			
		}else if(action == "retry"){
			
			Tween.call(Game.UI.pause._left);
			Game.restart();
			
		}else{
			Tween.call(Game.UI.pause._left);
		}
		
		Game._stage.uncache();
		
		Game.resume();
	}
	
	this._left = function(){
	
		Game._stage.removeChild(Game.UI.pause._obj);
	}
	
	this._update = function(){
		
		Game._pauseStage.update();
	
	}

}