function GameUI(){

	this._scrollY;
	this.menuItemHeight;
	this.menuItems;
	
	this._obj;				// Createjs Container for this 'stage'
	
	this.init = function(){
		var obj = new createjs.Container();
		obj.name = 'gameui';
		obj.alpha = 0;
		
		// If there is an old instance, remove from _stage, and add the new one
		var old = Game._stage.getChildByName(obj.name);
		if(!!old) Game._stage.removeChild(old);
		
		this._obj = obj;
		
		// Set game Variables
		Game.vars._lifeDrainPaused = false;
		Game.vars._currLifeTotal = 1000;
		Game.vars._spawnCount = 0;
		Game.vars._currentComboColorIndex = 0;
		
		Game.vars._hitScoresPlayed = { perfect: 0, good: 0, bad: 0, miss: 0 };
		Game.vars._currComboSchoresPlayed = { perfect: 0, good: 0, bad: 0, miss: 0 };
		Game.vars._currHitPercentage = 100;
		Game.vars._combosPlayed = [];
		Game.vars._score = 0;
		Game.vars._currScore = 0;
		Game.vars._currCombo = 0;
		
		// TODO : override with combo colors from the beatmaps if present
		Game.vars._comboColors = ['255,50,50', '0,200,0', '50,50,255', '230,200,50'];
		
		// Add the new UI
		Game._stage.addChild(this._obj);
	}
	
	this.load = function(){
		//set current to this UI
		Game.UI._current = this;
		
		Game.UI._current.init();
		
		Game.UI._current.create();
		
		Game.UI._current.onLoad();
	}
	
	this.create = function(){
		// Create all CreateJS stuff here // basic menu images should already be loaded from start!
		var _UI;
		var canvas = Game._stage.canvas;
		var scoreBG = new createjs.Bitmap(Game.loader.queue.getResult("score-bg"));
		var str = Game._beatmap.metadata['Artist'] + ' - ' + Game._beatmap.metadata['Title'] + '\n[' +  Game._beatmap.metadata['Version'] + ']';
		
		if(!this._obj){
			this.init();
		}
		
		_UI = this._obj;
		
		_UI.x = _UI.y = 0;
		_UI.width = canvas.width;
		_UI.height = canvas.height;
		_UI.zIndex = 50;
		
		_UI._txtCombo = new createjs.Text("0X", "64px Verdana", "#FFF");
		
		_UI._txtCombo.x =  10;
		_UI._txtCombo.y = canvas.height - _UI._txtCombo.getMeasuredHeight() - 15;
		
		_UI._txtPercent = new createjs.Text("100.00%", "48px Verdana", "#FFF");
		
		_UI._txtPercent.textAlign = 'right';
		
		_UI._txtPercent.x = canvas.width - 10;
		_UI._txtPercent.y = canvas.height - _UI._txtPercent.getMeasuredHeight() - 15;
		
		_UI._txtFPS = new createjs.Text("0.00FPS", "20px Verdana", "#FF0");
		
		_UI._txtFPS.textAlign = 'right';
		
		_UI._txtFPS.x = canvas.width - 10;
		_UI._txtFPS.y = canvas.height - _UI._txtPercent.getMeasuredHeight() - _UI._txtFPS.getMeasuredHeight() - 15;
		
		_UI._txtScore = new createjs.Text("0", "48px Verdana", "#FFF");
		
		_UI._txtScore.textAlign = 'right';
		
		_UI._txtScore.x = canvas.width - 10;
		_UI._txtScore.y = 5;
		
		_UI._txtSongTitle = new createjs.Text(str, "18px Verdana", "#FFF");
		
		_UI._txtSongTitle.x = 10;
		_UI._txtSongTitle.y = 40;
		
		_UI._progress = new createjs.Container();
		
		_UI._progress.x = canvas.width - 35;
		_UI._progress.y = 85;
		_UI._progress.height = _UI._progress.width = 2000;
		
		var p_bord = new createjs.Shape();
		p_bord.graphics.ss(3).s("rgba(255,255,255,0.8)").a(0,0,20,0,Math.PI*2);
		
		// SONG PROGRESS??!!?
		_UI._songProgress = new createjs.Shape();
		_UI._songProgress.graphics.f("rgba(255,255,255,0.5)").a(0,0,20,0,Math.PI*2);
		// END OF SONG PROGRESS?!?!
		
		_UI._progress.addChild(_UI._songProgress, p_bord);
		
		_UI.scoreBar = scoreBG;
		_UI.scoreBar.x = _UI.scoreBar.y = 0;
		
		_UI.lifeBarFill = new createjs.Shape();
		_UI.lifeBarFill.name = 'lifeBarFill';
		_UI.lifeBarFill.zIndex = 55;

		_UI.addChild(_UI._txtSongTitle, _UI._txtPercent, _UI._txtScore, _UI._txtCombo, _UI.scoreBar, _UI._txtFPS, _UI._progress, _UI.lifeBarFill);
		
		var gameStage = new createjs.Shape();
		gameStage.zIndex = 50;

		gameStage.graphics.rf(["rgba(0,0,0,0)","rgba(0,0,0,0.7)"], [0.3,1], 640, 360, 0, 640,360,720).drawRect(0,0,Game._stage.canvas.width,Game._stage.canvas.height);
		
		_UI.addChildAt(gameStage, 0);
		
		// Set invisible for fade in
		this._obj = _UI;
		this._obj.alpha = 0;
	}
	
	this.onLoad = function(){
		// What happens when this UI shows up? fade in? menu scrolls from side? etc. Fancy animations for Menu transitions
		Game.UI.setBackground();
		
		createjs.Tween.get(Game.UI._current._obj).to({alpha:1},1000).call(Game.startGame);
		
		Game.mediaplayer._scrollForVolume = true;
	}
	
	this.onLeave = function(nextStage){
		// Do specific shizzle for this UI item, when it should animate-out ( like scroll menu out of window )		
		Game.UI.setBackground(true);
		
		// Reset the played Beatmap
		Game._beatmap.reset();
		
		// Fadeout this._obj to alpha 0; and remove from Game._stage!
		createjs.Tween.get(Game.UI._current._obj).to({alpha:0},500).call(Game.helper.removeTweenObject);
		
		Game.mediaplayer._scrollForVolume = false;
	}
	
	this.updateLife = function(value){
	
		if(Game.vars._lifeDrainPaused) return;
		
		Game.vars._currLifeTotal += value;
		
		if(Game.vars._currLifeTotal > 1000){
			Game.vars._currLifeTotal = 1000;
		}
		
		if(Game.vars._currLifeTotal <= 0 ){
			// END GAME :(			
			//console.log("Actually....you died :(");
			
			Game.vars._currLifeTotal = 1000; // reset because I'm a good person
		}
		
		var pt_life = (Game.vars._currLifeTotal / 1000) * 636
		
		Game.UI.game._obj.lifeBarFill.graphics.clear();
		Game.UI.game._obj.lifeBarFill.graphics.ss(9, "round").s("white").mt(17,18).lt( 17 + pt_life, 18);

	}
	
	this.updateSongProgress = function(){
	
		if(!Game.mediaplayer.currentSong) return;
		
		Game.UI.game._obj._songProgress.graphics.clear();
		
		var p = Game.mediaplayer.currentSong.getPosition() / Game.mediaplayer.currentSong.getDuration();
		
		Game.UI.game._obj._songProgress.graphics.f('rgba(255,255,255,0.5)');//.a(0,0,20,0,(1-p) * Math.PI * 2);
		Game.helper.drawSegment(Game.UI.game._obj._songProgress, 0, 0, 20,(p*360) - 90, 270)
	}
	
	this._update = function(){
		
		if(!Game._isPaused){
			Game.UI.game.updateSongProgress();
			
			if(!Game.vars._lifeDrainPaused)
				Game.UI.game.updateLife(-(Game._beatmap.difficulty.HPDrainRate / 9));
				
			// TODO :: check this...
			if(!!Game._beatmap.hitobjectList){
				
				if(Game._beatmap._currentHitobject && (Game.mediaplayer.currentSong.getPosition() + 2050 + Game._beatmap.general.AudioLeadIn) >= Game._beatmap._currentHitobject.time + Game._beatmap.general.AudioLeadIn){
					
					//_currHitObj.isPlayed = true;
					Game._beatmap._currentHitobject.create();
					
					Game._beatmap._prevHitobject = Game._beatmap._currentHitobject;
					Game._beatmap._currentHitobject = Game._beatmap._nextHitobject;
					Game._beatmap._nextHitobject = (Game._beatmap._nextHitobject.index < Game._beatmap.hitobjectList.length - 1) ? Game._beatmap.hitobjectList[Game._beatmap._nextHitobject.index+1] : null;
				}
			}
		}
	}
}