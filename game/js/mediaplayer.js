MediaPlayer = function(){

	this._obj;

	this.currentSong;
	
	this._songStartTime;
	
	this.songList;

	this._scrollForVolume = false;
	
	this.init = function(){
		// Instantiate sound engine
		createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin, createjs.FlashPlugin]);
		createjs.Sound.alternateExtensions = ["ogg"];
		createjs.Sound.setVolume(0.5);
		
		this._obj = new createjs.Container();
		this._obj.name = "mediaplayer";
		this._obj.zIndex = 70;
		
		Game._stage.addChild(this._obj);
		
		$("#game_canvas").on('mousewheel', Game.mediaplayer.OnMouseScroll);
	}
	
	this.play = function(arg){
		
		if(!arg) { arg = "theme-song"; }
		
		if(!!this.currentSong)
			Game.mediaplayer.stop();
		
		Game.mediaplayer._songStartTime = createjs.Ticker.getEventTime();
		this.currentSong = createjs.Sound.play(arg);
		this.currentSong.volume = 0;
		
		createjs.Tween.get(Game.mediaplayer.currentSong).to({volume:0.5},1000);	
		
		this.currentSong.on("complete", this._onSongFinished);
	}
	
	this._onSongFinished = function(){
	
		if(Game.mediaplayer.currentSong != Game.mediaplayer.play())
			Game.UI.setBackground(true);
	}
	
	this.pause = function(){
		Game.mediaplayer.currentSong.pause();
	}
	
	this.resume = function(){
		Game.mediaplayer.currentSong.play();
	}
	
	this.stop = function(instantStop){
	
		if(instantStop){
			this.currentSong.stop();
		}else{
			createjs.Tween.get(Game.mediaplayer.currentSong).to({volume:0.0},1000).call(Game.mediaplayer.currentSong.stop);		
		}
	}
	
	this.OnMouseScroll = function(event, delta){
	
		if(Game.mediaplayer._scrollForVolume){
		
			var volumeChange = event.originalEvent.deltaY / 1000;
			
			var currVolume = createjs.Sound.getVolume();
			
			createjs.Sound.setVolume(currVolume - volumeChange);
			
			drawVolume();
			
			createjs.Tween.get(Game.mediaplayer._obj.getChildByName('volumeBar'), {override: true}).to({x: 0},200).wait(2000).to({x:40}, 200);
		}
	}
	
	this.createVolumeControl = function(){
	
		var volBar = new createjs.Container();
		volBar.name = 'volumeBar';
		volBar.x = 40;
	
		var volBarFill = new createjs.Shape();
		volBarFill.name = "volbarfill";
		
		var volumeBG = new createjs.Bitmap(Game.loader.queue.getResult("volume-bg"));
		volumeBG.x = Game._stage.canvas.width - volumeBG.image.width;
		volumeBG.y = Game._stage.canvas.height * 0.5 - volumeBG.image.height * 0.5;
		
		var canvas = Game._stage.canvas;
		
		var pt_bot = {x: canvas.width - 7, y: canvas.height * 0.5 + 110};
		var pt_rtop = {x: canvas.width - 7, y: (canvas.height * 0.5 - 110) + 220 * (1 - createjs.Sound.getVolume())};
		var pt_ltop = {x: (canvas.width - 14) + 7 * (1 - createjs.Sound.getVolume()), y:(canvas.height * 0.5 - 110) + 220 * (1 - createjs.Sound.getVolume())};
		
		volBarFill.graphics.beginFill("white").mt(pt_bot.x,pt_bot.y).lt(pt_ltop.x,pt_ltop.y).lt(pt_rtop.x,pt_rtop.y).es();
		
		var oldVolBar = Game.mediaplayer._obj.getChildByName('volumeBar');
		if(oldVolBar)
			Game.mediaplayer._obj.removeChild(oldVolBar);
		
		volBar.addChild(volumeBG, volBarFill);
		
		Game.mediaplayer._obj.addChild(volBar);
		
		Game._stage.sortChildren(Game.helper.sortByZ);
	
	}
	
	function drawVolume(){
	
		var vol = Game.mediaplayer._obj.getChildByName('volumeBar');

		var volBarFill = new createjs.Shape();
		volBarFill.name = "volbarfill";
		
		var canvas = Game._stage.canvas;
		
		var pt_bot = {x: canvas.width - 7, y: canvas.height * 0.5 + 110};
		var pt_rtop = {x: canvas.width - 7, y: (canvas.height * 0.5 - 110) + 220 * (1 - createjs.Sound.getVolume())};
		var pt_ltop = {x: (canvas.width - 14) + 7 * (1 - createjs.Sound.getVolume()), y:(canvas.height * 0.5 - 110) + 220 * (1 - createjs.Sound.getVolume())};
		
		volBarFill.graphics.beginFill("white").mt(pt_bot.x,pt_bot.y).lt(pt_ltop.x,pt_ltop.y).lt(pt_rtop.x,pt_rtop.y).es();
		
		var fill = vol.getChildByName('volbarfill');
		if(fill)
			vol.removeChild(fill);
		
		vol.addChild(volBarFill);
		
		Game.mediaplayer._obj;
		
		Game._stage.sortChildren(Game.helper.sortByZ);
	}
}