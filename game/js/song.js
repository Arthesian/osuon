Song = function(){

	// Properties

	this.id;
	this.file;
	this.beatmap;
	this.title;
	this.folder;
	this.beatmaps = [];
	
	this._isSongFileLoaded = false;
	
	// Methods for song
	
	this._loadMusicFile(){
		if(this._isSongFileLoaded) { this._musicFileLoaded(); return; }
		
	}
	
	this._musicFileLoaded(){
		this._isSongFileLoaded = true;
	}
}