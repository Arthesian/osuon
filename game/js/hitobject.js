// JavaScript Document

function HitObject(){
	this._obj;
	
	this.isClicked = false;
	this.isPlayed = false;				// default
	this.isStacked = false;				// default
	
	this.x;
	this.y;
	this.origX;
	this.origY;
	this.time;
	this.hitSounds;
	this.isNewCombo;
	this.type;
	
	this.parseData = function(data){
		var obj_data = data.split(',');
		
		this.x = parseInt(obj_data[0]);		// x // can be altered
		this.y = parseInt(obj_data[1]);		// y // can be altered
		this.origX = parseInt(obj_data[0]);	// x
		this.origY = parseInt(obj_data[1]);	// y
		this.time = parseInt(obj_data[2]);		// time in MS since song start
		this.hitSounds = this.getHitObjectSounds(parseInt(obj_data[4]));		// getHitsoundIndexNumbers ( array )
		this.isNewCombo = this.getHitObjectNewCombo(parseInt(obj_data[3]));		// true/false if starts net combo
		this.type = this.getHitObjectType(parseInt(obj_data[3]));				// type :: hitcircle / spinner / slider
		
		switch(this.type){
			case 'spinner':
				this.endTime = parseInt(obj_data[5]);
			break;
			case 'slider':
				this.points = this.getSliderPoints(obj_data);
				this.repeats = parseInt(obj_data[6])		
			break;
		}
		
		return this;
	}
	
	this.getSliderPoints = function(obj_data){
		var pdata = obj_data[5].split('|');
		var points = [];
		
		points[0] = Game.helper.osuXYtoGameXY(obj_data[0],obj_data[1]);
		
		for(i = 0; i < pdata.length; i++){
			var point = pdata[i].split(':');
			
			if(point.length == 1) continue; // bezier type
			
			points.push(Game.helper.osuXYtoGameXY(point[0], point[1]));
		}
		
		return points;
	}

	this.getHitObjectNewCombo = function(int){
		switch(int){
			case 5:
			case 6:
			case 12:
				return true;
				break;
			default:
				return false;
		}
	}

	this.getHitObjectType = function(int){
		switch(int){
			case 5:
			case 1:
				return 'hitcircle';
				break;
			case 6:
			case 2:
				return 'slider';
				break;
			case 12:
			case 8:
				return 'spinner';
				break;
		}
	}

	this.getHitObjectSounds = function(int){
		switch(int){
			case 0:
				return [0];
				break;
			case 1:
				return [1];
				break;
			case 2:
				return [2];
				break;
			case 3:
				return [1,2];
				break;
			case 4:
				return [3];
				break;
			case 5:
				return [1,3];
				break;
			case 6:
				return [2,3];
				break;
			case 7:
				return [1,2,3];
				break;
			case 8:
				return [4];
				break;
			case 9:
				return [1,4];
				break;
			case 10:
				return [2,4];
				break;
			case 11:
				return [1,2,4];
				break;
			case 12:
				return [3,4];
				break;
			case 13:
				return [1,3,4];
				break;
			case 14:
				return [2,3,4];
				break;
			case 15:
				return [1,2,3,4];
				break;
		}
	}
	
	// CREATE VISIBLE HITOBJECTS :: TODO :: Hitobjects get added to _stage directly.... preferably add to _gameStage container
	
	this.create = function(){
		if(this.type == 'spinner') this.createSpinner();
		
		if(this.type == 'hitcircle') this.createHitcircle();
		
		if(this.type == 'slider') this.createSlider();
	}

	this.createHitcircle = function(){
		
		var circle = this.createHitCircleCircle(++Game.vars._spawnCount, true, false);

		circle.alpha = 0;
		
		if(Game._beatmap._prevHitobject){
		
			var old = Game._beatmap._prevHitobject;
		
			if(old.origX == this.origX && old.origY == this.origY) {
				
				var xy = Game.helper.osuXYtoGameXY(old.x, old.y);
				
				circle.x = xy.x + 10;
				circle.y = xy.y + 10;		
			}				
		}
		
		//circle.addEventListener("click", clickedHitcircle);

		createjs.Tween.get(circle).wait(1500 - Game._beatmap.AR).to({alpha: 1}, 500).wait(Game._beatmap.AR + 300).to({alpha:0},100).call(this._missed);
		
		this._obj = circle;
		
		Game.UI.game._obj.addChildAt(this._obj,1);
	}

	this.createSpinner = function(){
		
		var xy = Game.helper.osuXYtoGameXY(this.x, this.y);
		var x = xy.x;
		var y = xy.y;
		
		this.isPlayed = true;
		this.isClicked = true; // FREE HIT :D
		
		var obj = new createjs.Container();
		
		obj.mouseEnabled = false;
		
		var ac = new createjs.Bitmap(Game.loader.queue.getResult("spin-ac"));
		var top = new createjs.Bitmap(Game.loader.queue.getResult("spin-top"));
		var bot = new createjs.Bitmap(Game.loader.queue.getResult("spin-bot"));
		var mid = new createjs.Bitmap(Game.loader.queue.getResult("spin-mid"));
		var bord = new createjs.Bitmap(Game.loader.queue.getResult("spin-bord"));
		var rpm = new createjs.Bitmap(Game.loader.queue.getResult("spin-rpm"));
		
		var txtSpin = new createjs.Bitmap(Game.loader.queue.getResult("spin-txtSpin"));
		var txtClear = new createjs.Bitmap(Game.loader.queue.getResult("spin-txtClear"));
		
		var bg = new createjs.Shape();
		bg.graphics.f("rgba(0,0,0,0.8)").dr(0,0,Game._stage.canvas.width,Game._stage.canvas.height);
		  
		if(this.isNewCombo){
			Game.vars._spawnCount = 0;
			if(Game.vars._currentComboColorIndex < (Game.vars._comboColors.length - 1)){
				Game.vars._currentComboColorIndex++;
			}else{
				Game.vars._currentComboColorIndex = 0;
			}
		}
		
		ac.scaleX = ac.scaleY = 3;								// set spinner approach circle initial scale
		
		txtSpin.x = txtClear.x = x;								// set position of spinner and approach circle
		txtSpin.y = txtClear.y = y;
		
		mid.regX = mid.image.width * 0.5;
		mid.regY = mid.image.height * 0.5;
		
		bord.regX = bord.image.width * 0.5;
		bord.regY = bord.image.height * 0.5;
		
		txtClear.regX = txtClear.image.width * 0.5;
		txtClear.regY = txtClear.image.height * 0.5;
		
		txtSpin.regX = txtSpin.image.width * 0.5;
		txtSpin.regY = txtSpin.image.height * 0.5;
		
		bot.regX = top.regX = top.image.width * 0.5;			// center spinner
		bot.regY = top.regY = top.image.height * 0.5;
		
		ac.regX = ac.image.width * 0.5;							// center approach circle
		ac.regY = ac.image.height * 0.5;
		
		rpm.x = x;
		rpm.y = Game._stage.canvas.height - rpm.image.height;
		
		rpm.regX = rpm.image.width * 0.5;
		rpm.regY = 0;
		
		obj.hitObj = this;									// add hitObject to visible spinner
		
		createjs.Tween.get(top, {loop:true}).to({rotation:360}, 750);
		
		obj.alpha = 0;
		ac.alpha = 0;
		
		createjs.Tween.get(ac).wait(2200 - Game._beatmap.AR).to({alpha: 0.8, scaleX: 0.1, scaleY: 0.1}, this.endTime - this.time - 100).to({alpha:0}, 100); 
		createjs.Tween.get(obj).wait(2200 - Game._beatmap.AR).to({alpha: 1}, 500).wait( this.endTime - this.time - 600).to({alpha:0},100).call(this._missed);
		
		obj.x = x;
		obj.y = y;
		
		obj.scaleX = 0.8;
		obj.scaleY = 0.8;
		
		obj.addChild(bg, bot, top, ac, bord, mid);
		
		//_stage.addChildAt(obj, 1); // insert at index 1 { should be play level niveau }
		
		txtClear.alpha = 0;
		txtSpin.alpha = 0;
		txtClear.mouseEnabled = txtSpin.mouseEnabled = false;
		
		createjs.Tween.get(txtSpin).wait(1800 - Game._beatmap.AR).to({alpha: 1}, 200).wait(500).to({alpha:0},100).call(Game.helper.removeTweenObject);
		createjs.Tween.get(txtClear).wait(2200 - Game._beatmap.AR + this.endTime - this.time).to({alpha: 1}, 200).wait(300).to({alpha:0},100).call(Game.helper.removeTweenObject); 
		rpm.alpha = 0;
		createjs.Tween.get(rpm).wait(2200 - Game._beatmap.AR).to({alpha:1}, 200).wait( this.endTime - this.time - 300).to({alpha:0},100).call(Game.helper.removeTweenObject);
		bg.alpha = 0;
		createjs.Tween.get(bg).wait(2000 - Game._beatmap.AR).to({alpha:1}, 400).wait( this.endTime - this.time - 300).to({alpha:0},400).call(Game.helper.removeTweenObject);
		
		// Add Spinner to gameUI container
		var cont = new createjs.Container();
		cont.addChild(bg, rpm, obj);
		
		this._obj = cont;
		
		Game.UI.game._obj.addChildAt(this._obj,0);
	}

	this.createSlider = function(){
		
		var xy = Game.helper.osuXYtoGameXY(this.x, this.y);
		
		var x = xy.x;
		var y = xy.y;
		
		this.isPlayed = true;
		this.isClicked = true; // FREE HIT :D
		this.mouseEnabled = false;
		
		var circleSize = 200;
		
		var fs = (circleSize / 200) * 72;
		
		var obj = new createjs.Container();
		var ac = new createjs.Bitmap(Game.loader.queue.getResult("approachCircle"));
		
		// TODO :: Check this function! stage variables used... not sure
		if(this.isNewCombo){
			Game.vars._spawnCount = 0;
			if(Game.vars._currentComboColorIndex < (Game.vars._comboColors.length - 1)){
				Game.vars._currentComboColorIndex++;
			}else{
				Game.vars._currentComboColorIndex = 0;
			}
		}
		
		if(Game._beatmap._prevHitobject){
		
			var old = Game._beatmap._prevHitobject;
		
			if(old.origX == this.origX && old.origY == this.origY) {
				
				var xy = Game.helper.osuXYtoGameXY(old.x, old.y);
				
				obj.x += 10;
				obj.y += 10;		
			}				
		}
		
		ac.scaleX =  4;
		ac.scaleY =  4;
		
		ac.x = x;
		ac.y = y;
		ac.regY = ac.image.width * 0.5;
		ac.regX = ac.image.height * 0.5;
		
		obj.hitObj = this;
		
		obj.alpha = 0;
		
		var bezier = this.getSliderBezier(this);
		
		var startCircle = this.createHitCircleCircle(++Game.vars._spawnCount, true, false);
		var endCircle = this.createHitCircleCircle(null , false, this.points[this.points.length - 1]);
		
		obj.addChild(bezier.bezier, startCircle, endCircle, bezier.ball);
		
		Game.UI.game._obj.addChildAt(obj, 1);
		
		//_stage.addChildAt(obj, 1); // insert at index 1 { should be play level niveau } // TODO :: don't add directly to stage
		
		ac.alpha = 0;
		
		bezier.ball.alpha = 0;
		// hitcircle 
		createjs.Tween.get(bezier.ball).wait(2000).to({alpha: 1}, 100);
		//createjs.Tween.get(ac).wait(_beatmap.approachRate + 700).to({alpha: 0.6, scaleX: 1, scaleY: 1}, _beatmap.approachRate).to({alpha:0}, 100); 
		createjs.Tween.get(obj).wait(1500 - Game._beatmap.AR).to({alpha: 1}, 500).wait(Game._beatmap.AR + bezier.time).to({alpha:0},100).call(this._missed);
	}
	
	this._duration = 0;
	
	this.getSliderBezier = function(){
	
		var points = this.points;
		var repeats = this.repeats; // default = 1 = no repeat
		
		//
		// LE PROBLEMO STARTS HERE! 
		//
		var absLength = 0; 
		
		var BPM = Game._beatmap.BPM;            // variable Beats per Minute (Song)
		var sliderSpeed = 0.9;    // Variable multiplier
		
		// Calculate pathlength between the points
		for (i = 0; i < points.length - 1; i++){
			absLength += Game.helper.pythagoras(Math.abs(points[i].x - points[i+1].x), Math.abs(points[i].y - points[i+1].y));
		}
		
		var relLength = (absLength*(Math.pow(0.90, points.length))) / sliderSpeed;
		
		var beats = relLength * repeats * 4; // 0.25???? random number
		// Some static number
		
		var beatTime = Game._beatmap.timingPoints[0].beatTime; // Get the first Tmingpoint atm? fuck shit
		
		var sliderTime = beatTime * beats * 0.001;
		
		this._duration = sliderTime;
		//
		// LE PROBLEMO ENDS HERE!
		//

		// Declare vars
		var slider = new createjs.Container();
		var sliderBorder = new createjs.Shape();
		var sliderBorderMask = new createjs.Shape();
		var sliderFill = new createjs.Shape();
		var sliderFillMask = new createjs.Shape();
		
		var scale = Game._beatmap.CS / 128;

		// create the start and end-circle mask for the colored fill
		sliderFillMask.graphics.beginFill("red").arc(points[points.length - 1].x, points[points.length - 1].y,62 * scale,0,Math.PI*2,true).drawRect(0,0,10000,2000).arc(points[0].x, points[0].y,62 * scale,0,Math.PI*2,true);   
	   
		// go to first point for Black, 'white', and colored bezier
		var outerSize = 128 * scale;
		var midSize = 114 * scale;
		var innerSize = 110 * scale;
		
		// WHITE BORDER, BLACK INSET
		sliderBorder.graphics.beginStroke("white").setStrokeStyle(outerSize).moveTo(points[0].x, points[0].y);
		sliderBorderMask.graphics.beginStroke("rgb(70,70,70)").setStrokeStyle(midSize).moveTo(points[0].x, points[0].y);
		sliderFill.graphics.beginStroke('rgba(' + Game.vars._comboColors[Game.vars._currentComboColorIndex] + ',0.8)').setStrokeStyle(innerSize).moveTo(points[0].x, points[0].y);
		
		if(points.length > 2){
		// Loop throught 'middle points'
			for (i = 0; i < points.length - 2; i ++) // WAS SUPPOSED TO BE i = 1;
			{
				var xc = (points[i].x + points[i + 1].x) / 2;
				var yc = (points[i].y + points[i + 1].y) / 2;
				
				// Create center beziers for Black, 'white', and colored bezier
				sliderBorder.graphics.quadraticCurveTo(points[i].x, points[i].y, xc, yc);
				sliderBorderMask.graphics.quadraticCurveTo(points[i].x, points[i].y, xc, yc);
				sliderFill.graphics.quadraticCurveTo(points[i].x, points[i].y, xc, yc);
			}
		}else{
			i = 0;
		}
		
		// curve through the last two points for Black, 'white', and colored bezier
		sliderBorder.graphics.quadraticCurveTo(points[i].x, points[i].y, points[i+1].x,points[i+1].y);
		sliderBorderMask.graphics.quadraticCurveTo(points[i].x, points[i].y, points[i+1].x,points[i+1].y);
		sliderFill.graphics.quadraticCurveTo(points[i].x, points[i].y, points[i+1].x,points[i+1].y);
		
		//
		// CREATE THE SLIDER BALL ::
		//
		
		var ball = new createjs.Shape();
		ball.compositeOperation = 'source-over';
		ball.graphics.f('rgba(' + Game.vars._comboColors[Game.vars._currentComboColorIndex] + ',1)')
			.arc(0,0,55,0,2*Math.PI).ss(2).s('black').mt(0,-55).lt(0,55);
			
			
		ball = new createjs.Bitmap(Game.loader.queue.getResult("sliderball"));
		ball.regX = ball.image.width / 2;
		ball.regY = ball.image.height / 2;
		ball.scaleX = ball.scaleY = scale;
		// MOVE THE BALL ALONG SLIDER

		// GOT TO CALC THIS SHIT REAL GOOD
		var delay = 2; //(2000 - _beatmap.approachRate) + _beatmap.approachRate / 1000;
		var length = 0.3; //(_beatmap.approachRate + 300) - _beatmap.approachRate / 1000;
		
		TweenMax.to(ball,0,{x:points[0].x, y:points[0].y});
		TweenMax.to(ball, (sliderTime * 0.001) / repeats, {bezier: {type:"soft",values:points,autoRotate:true}, ease:Linear.easeNone, yoyo:true, repeat: (repeats - 1), delay:delay});
		
		// Add sliders to container object
		slider.addChild(sliderBorder, sliderBorderMask, sliderFill);
		slider.mask = sliderFillMask;	
		slider.alpha = 0.75;
		
		var obj = {};
		
		obj.bezier = slider;
		obj.ball = ball;
		obj.time = sliderTime;
		
		return obj;
	}

	this.createHitCircleCircle = function(text, createAc, sliderEnd){
		
		var circle = new createjs.Container();
		
		circle.hitObj = this;
		
		var xy = Game.helper.osuXYtoGameXY(this.x, this.y);
		
		if(sliderEnd){
			xy = sliderEnd;
		}
		
		var size = 122;
		var mid = size * .5;
		
		var img = new createjs.Bitmap(Game.loader.queue.getResult("hitCircle"));
		img.regX = img.regY = mid = img.image.width * 0.5;
		
		var color = new createjs.Shape();
		var c = 'rgba(' + Game.vars._comboColors[Game.vars._currentComboColorIndex] + ',0.8)'; // Create RGBA 0.8
		
		color.graphics.beginFill(c).drawCircle(0,0,mid - 5); // -3 correction, might else oversize the img
		
		// TEST
		//img = new createjs.Shape();
		//img.graphics.rf(['rgba(0,0,0,0)', 'rgba(0,0,0,0)', 'rgba(0,0,0,0.8)', 'rgba(220,220,220,0.9)','rgba(220,220,220,0.9)'], [0,0.85,0.9,0.92,1], 0, 0, 0, 0, 0, 64).dc(0,0,64);
		// END TEST
		
		var content = new createjs.Text();
		var content2 = new createjs.Text();
		
		if(text != null){
			if(text == 0){
				// repeat logo
			}else{
				var fs = (Game._beatmap.CS / 200) * 72;
				
				content = new createjs.Text(text, fs + "px Verdana", "#FFF");

				content.textAlign = 'center';
				content.textBaseline = 'middle';
				
				content2 = new createjs.Text(text, fs + "px Verdana", "rgba(0,0,0,0.8)");
				content2.outline = 2;
				content2.textAlign = 'center';
				content2.textBaseline = 'middle';
			}
		}
		
		var ac = new createjs.Bitmap();
		
		if(createAc){
			ac = new createjs.Bitmap(Game.loader.queue.getResult("approachCircle"));
			
			ac.x = ac.y = 0;
			ac.regX = ac.regY = mid;
			
			ac.scaleX = ac.scaleY = 5;
			ac.alpha = 0;
			
			ac.mouseEnabled = false;
			
			createjs.Tween.get(ac).wait((1500 - Game._beatmap.AR)).to({alpha: 1, scaleX: img.scaleX, scaleY: img.scaleY}, Game._beatmap.AR + 375).to({alpha:0}, 100);
		}

		var scale = Game._beatmap.CS / size; 

		circle.x = xy.x;
		circle.y = xy.y;
		circle.scaleX = circle.scaleY = scale;
		
		
		this._createAutoCursor(xy, sliderEnd)
		
		// LINE TO NEXT OBJECT::
		var comboLine = new createjs.Shape();
		
		if((Game._beatmap._nextHitobject && Game._beatmap._nextHitobject.isNewCombo == false) && (this.type == 'hitcircle' || sliderEnd)){  // check if hitcircle or sliderend 
		
			var newXY = Game.helper.osuXYtoGameXY(Game._beatmap._nextHitobject.x, Game._beatmap._nextHitobject.y);
			
			comboLine.graphics.ss(8, "round").s("rgba(255,255,255,0.5)").mt(xy.x, xy.y).lt(newXY.x, newXY.y);
			
			comboLine.alpha = 0;
		
			createjs.Tween.get(comboLine).wait(1000).to({alpha:1}, 200).wait(1000).to({alpha:0},200).call(Game.helper.removeTweenObject);

			Game.UI.game._obj.addChildAt(comboLine, 1);
		}
		
		// END OF LINE TO NEXT OBJECT

		circle.addChild(color, img, content2, content, ac);
		
		return circle;
	}
	
	
	this._createAutoCursor = function(xy, sliderEnd){
		
		var dur = this._duration || 0;
		
		var helpCursor = new createjs.Shape();
		
		if(Game._beatmap._nextHitobject){
			var newXY = Game.helper.osuXYtoGameXY(Game._beatmap._nextHitobject.x, Game._beatmap._nextHitobject.y);
			
			helpCursor.graphics.rf(["rgba(255,255,255,1)", "rgba(255,0,0,1)","rgba(255,0,0,0.7)"], [0.3, 0.6,1], 0, 0, 0, 0,0,15).a(0,0,15,0,Math.PI * 2);
			
			helpCursor.zIndex = 50;
			
			helpCursor.x = xy.x;
			helpCursor.y = xy.y;
			
			helpCursor.alpha = 0;
			
			if ((this.type != 'slider'))
				createjs.Tween.get(helpCursor).wait(1900).to({alpha:1},0).to({x: newXY.x, y:newXY.y}, Game._beatmap._nextHitobject.time - this.time).call(Game.UI.game._obj.removeChild, [helpCursor], Game.UI.game._obj);
			else if (sliderEnd){
				createjs.Tween.get(helpCursor).wait(1900 + dur).to({alpha:1},0).to({x: newXY.x, y:newXY.y}, Game._beatmap._nextHitobject.time - this.time - dur).call(Game.UI.game._obj.removeChild, [helpCursor], Game.UI.game._obj);
			}else
			{
				// If slider
				newXY = this.points[this.points.length -1];
				createjs.Tween.get(helpCursor).wait(1900).to({alpha:1},0).to({x: newXY.x, y:newXY.y}, dur).call(Game.UI.game._obj.removeChild, [helpCursor], Game.UI.game._obj);
			}
			
			Game.UI.game._obj.addChild(helpCursor);
		}
	
	}
	
	// HIT OBJECT EVENTS

	this._missed = function(event){
		
		var obj = event.target;
		
		// FAKE :: if object was hit :D Don't act like it was missed
		if(obj.hitObj.isClicked){Game.UI.game._obj.removeChild(obj); return;}
		
		var img = new createjs.Bitmap(Game.loader.queue.getResult("hit-0"));
		
		img.x = obj.x;
		img.y = obj.y;
		
		img.regX = img.image.width * 0.5;
		img.regY = img.image.height * 0.5;
		
		img.mouseEnabled = false;
		
		Game.UI.game._obj.addChildAt(img,1);
		
		//_stage.addChildAt(img, 1);
		
		Game.vars._hitScoresPlayed.miss++;
		
		Game.UI.game.updateLife(-((Game._beatmap.difficulty.HPDrainRate) / 9) * 200);
		
		//calculateScore();
		//calculateHitPercentage();
		
		//updateScore();
		
		// reset combo
		Game.vars._currCombo = 0;
		Game.vars._currComboSchoresPlayed = { perfect: 0, good: 0, bad: 0 };
		Game.UI.game._obj._txtCombo.text = Game.vars._currCombo + 'X';
		
		createjs.Tween.get(img).wait(200).to({alpha: 0}, 300).call(Game.helper.removeTweenObject);
		
		Game.UI.game._obj.removeChild(obj);
	}

	this._hit = function(event){
		
		var obj = event.target.parent;
		
		if(obj.hitObj.type == 'spinner' || obj.hitObj.type == 'slider' || obj.hitObj.isClicked) return;
		
		obj.hitObj.isClicked = true;
		
		obj.mouseChildren = false;
		obj.mouseEnabled = false;
		obj.removeAllEventListeners();
		
		createjs.Tween.get(obj).to({scaleX: 0.95, scaleY: 0.95}, 50).to({alpha: 0, scaleX: 1.2, scaleY: 1.2}, 150).call(Game.helper.removeTweenObject);
		
		// up combo
		_currCombo++;
		_UI._txtCombo.text = _currCombo + 'X';
		
		// Load correct image depending on Hittiming vs obj.time !!
		var tt = obj.hitObj.time + _beatmap.leadInTime;
		var gt = _currStageTime - _songStartTime; // goaltime
		
		var img;
		var sound = "hit-normal"; // TODO :: set correct sounds for hit ( and adjust volume )
		
		var missed = false;
		
		if(tt < gt + 100 && tt > gt - 100){
			 img = new createjs.Bitmap(_loadQueue.getResult("hit-300"));
			 _hitScoresPlayed.perfect++;
			 _currComboSchoresPlayed.perfect++;
			 
			 updateLife(((10 - _beatmap["Difficulty"]["HPDrainRate"]) / 9) * 200);
		}else if( tt < gt + 200 && tt > gt - 200){
			img = new createjs.Bitmap(_loadQueue.getResult("hit-100"));
			_hitScoresPlayed.good++;
			_currComboSchoresPlayed.good++;
			
			updateLife(((10 - _beatmap["Difficulty"]["HPDrainRate"]) / 9) * 100);
		}else if( tt < gt + 300 && tt > gt - 300){
			img = new createjs.Bitmap(_loadQueue.getResult("hit-50"));
			_hitScoresPlayed.bad++;
			_currComboSchoresPlayed.bad++;
			
			updateLife(((10 - _beatmap["Difficulty"]["HPDrainRate"]) / 9) * 50);
		}else{
			img = new createjs.Bitmap(_loadQueue.getResult("hit-0"));
			_hitScoresPlayed.miss++;
			sound = "combobreak";
			missed = true;
			
			updateLife(-((10 - _beatmap["Difficulty"]["HPDrainRate"]) / 9) * 100);
		}
		
		calculateScore();

		if(missed){
			updateScore();
			_currCombo = 0;
			_currComboSchoresPlayed = { perfect: 0, good: 0, bad: 0 };
		}
		
		calculateHitPercentage();
		
		
		// Load correct sound depending on Hitsound of obj.
		createjs.Sound.play(sound);
		
		img.x = obj.x;
		img.y = obj.y;
		
		img.regX = img.image.width * 0.5;
		img.regY = img.image.height * 0.5;
		
		img.mouseEnabled = false;
		
		_stage.addChildAt(img, 1);
		
		createjs.Tween.get(img).wait(200).to({alpha: 0}, 300).call(Game.helper.removeTweenObject);
	}
}