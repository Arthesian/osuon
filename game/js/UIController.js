function UIController(){
	this.main;
	this.game = new GameUI();
	this.songList = new SongListUI();
	this.pause = new PauseUI();
	this.pauseOverlay;
	
	this.transitionTime = 500;
	
	this._current;
	
	this.init = function(){
		// Init?
		//this.main = new mainUI();

		this.game.init();
		this.songList.init();
	}
	
	this.load = function(arg){
	
		if(!!Game.UI._current) Game.UI._current.onLeave();
	
		switch(arg){
			case 'main':
				Game.UI.main.load();
				break;
			case 'songList':
				Game.UI.songList.load();
				break;
			case 'game':
				Game.UI.game.load();
				break;
			case 'pause':
				Game.UI.pause.load();
			default:
				Game.UI.songList.load();
				break;
		}
	}
	
	this._onGamePause = function(){
		// Show Pause Menu :D
		if(Game.UI._current == Game.UI.game){
			Game.UI.pause.load();
		}
	}
	
	this._onGameResume = function(){
		// Hide Pause Menu :D
		// it handles it himself :( 
	}
	
	this.setBackground = function(reset){
		// Background Stuff
		var bg;
		
		if(reset){
			bg = new createjs.Bitmap(Game.loader.queue.getResult("background_main"));
			
			bg = new createjs.Shape();
			bg.graphics.f("rgb(0,127,255)").drawRect(0,0,Game._stage.canvas.width,Game._stage.canvas.height);
			bg.image = {};
			bg.image.height = Game._stage.canvas.height;
			bg.image.width = Game._stage.canvas.width;
		}else{
			bg = new createjs.Bitmap(Game.loader.queue.getResult("background"));
		}
		
		bg.name = "background";
		bg.zIndex = 0;
		
		var scaleY = Game._stage.canvas.height / bg.image.height;
		var scaleX = Game._stage.canvas.width / bg.image.width;
		
		var scale;

		scale = (scaleY > scaleX) ? scaleY : scaleX;		

		
		bg.scaleX = bg.scaleY = scale;
		
		var offsetY = (Game._stage.canvas.height - bg.image.height * scale) / 2;
		var offsetX = (Game._stage.canvas.width - bg.image.width * scale) / 2; 
		
		bg.x = offsetX;
		bg.y = offsetY;
		
		// Remove old background if already set
		var oldBG = Game._stage.getChildByName("background");
		if(oldBG){
			createjs.Tween.get(oldBG).to({alpha: 0},1000).call(Game.helper.removeTweenObject);
		}
		
		Game._stage.addChildAt(bg, 0);
		// End of Background Stuff
		Game._stage.sortChildren(Game.helper.sortByZ);
	}
	
	this._isEmittingStars = false;
	
	this._emitStars = function(){
		if(this._isEmittingStars) {
		
			var star = Game._cursor._starObj.clone();
			star.x = (Math.random() * Game._stage.canvas.width);
			star.y = (Math.random() * Game._stage.canvas.height);
			star.rotation = Math.random() * 360;
			star.zIndex = 1;

			// Loop this function
			setTimeout(function(){
				Game.UI._emitStars();
			}, 100);
			
			Game._stage.addChild(star);
			
			Game._stage.sortChildren(Game.helper.sortByZ);
			
			createjs.Tween.get(star).to({rotation: star.rotation + 360}, 2000);
			createjs.Tween.get(star).to({alpha: 1}, 500).to({alpha: 0}, 1500).call(Game.helper.removeTweenObject);
			//createjs.Tween.get(star).to({x: star.x + diffX, y: star.y + diffY, }, 500, createjs.Ease.cubicIn).call(Game.helper.removeTweenObject);
		}
	}
	
	this.startEmittingStars = function(){
		
		this._isEmittingStars = true;
		
		this._emitStars();
	}
	
	this.stopEmittingStars = function(){
		this._isEmittingStars = false;
	}
	
	// TODO :: check background? slide-in/-out when not used? etc.
	this.drawVolume = function(){
		var volume = createjs.Sound.getVolume();
		var canvas = Game._stage.canvas;
		var volBar = new createjs.Container();
		var volBarFill = new createjs.Shape();
		
		var pt_bot = {x: canvas.width - 7, y: canvas.height * 0.5 + 110};
		var pt_rtop = {x: canvas.width - 7, y: (canvas.height * 0.5 - 110) + 220 * (1 - volume)};
		var pt_ltop = {x: (canvas.width - 14) + 7 * (1 - volume), y:(canvas.height * 0.5 - 110) + 220 * (1 - volume)};
		
		volBarFill.graphics.beginFill("white").mt(pt_bot.x,pt_bot.y).lt(pt_ltop.x,pt_ltop.y).lt(pt_rtop.x,pt_rtop.y).es();
		
		volBar.addChild(volBarFill);
		volBar.name = 'volumeBar';
		volBar.zIndex = 55;
		
		// If already on stage, remove old bar
		var oldVolBar = Game._stage.getChildByName(volBar.name);
		if(oldVolBar)
			Game._stage.removeChild(oldVolBar);
		
		Game._stage.addChild(volBar);
	}
	
	this._update = function(){
		if(!!this._current) Game.UI._current._update();
	}
}