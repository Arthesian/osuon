Loader = function(){
	
	// Properties
	this.queue;
	
	this.isLoading = false;
	this.progress = 0;
	
	this._obj;
	
	// Initialize
	
	this.init = function(){
		
		// Define LoadQueue
		this.queue = new createjs.LoadQueue(true);
		this.queue.installPlugin(createjs.Sound, createjs.WebAudioPlugin, createjs.HTMLAudioPlugin);
		
		this.queue.addEventListener("complete", this.hideProgressBar);
		this.queue.addEventListener("progress", this.updateProgressBar);
	}
	
	// ProgressBar Methods
	
	this._createProgressBar = function(){
		// If the progressbar already exists, reset values and return
		if(!!Game.loader._obj){ Game.loader.progress = 0; }
	
		var pb = new createjs.Container();
		pb.name = "progressbar";
		pb.zIndex = 1;
		// Create ._obj as Createjs Container
		
		var bar = new createjs.Shape();
		bar.name = "bar";
		var txt = new createjs.Text("0%", "20px Arial", "#FAA");
		txt.name = "txt";
		
		pb.addChild(bar, txt);
		
		pb.alpha = 0;
		
		Game.loader._obj = pb;
		
		var old = Game._stage.getChildByName(pb.name);
		if(!!old) Game._stage.removeChild(old);
		
		Game._stage.addChild(Game.loader._obj);
		
		Game._stage.sortChildren(Game.helper.sortByZ);
		
		Game.loader.showProgressBar();
		// TODO :: create ProgressBar
	}
	
	this.updateProgressBar = function(event){
		// if no progressbar, create new one ; else take existing one
		var pb;
			
		if(!Game.loader._obj) {
			Game.loader._createProgressBar();
		}
		
		pb = Game.loader._obj;
		
		var bar = pb.getChildByName("bar");
		var txt = pb.getChildByName("txt");
		
		// Clear ProgressBar
		bar.graphics.clear();
		
		// update percentage
		bar.zIndex = 10;
		txt.zIndex = 11;
		
		var text = (event.progress*100).toFixed(2);
		
		txt.text = text + '%';
		
		// Draw the progress bar
		var width = 316, height = 20;
		var loaded = width*(event.progress / event.total);
		
		var x = (Game._stage.canvas.width * 0.5) - ( width * 0.5);
		var y = (Game._stage.canvas.height * 0.6) - ( height * 0.5);
		
		bar.graphics.beginStroke("#FAA").drawRect( x, y, width, height);
		bar.graphics.beginFill("#FCC").drawRect( x, y, loaded, height);
		
		txt.x = (Game._stage.canvas.width * 0.5) - 35;
		txt.y = y + 30;
		
		pb.removeAllChildren();
		pb.addChild(bar, txt);
		
		// Set _obj
		this._obj = pb;
	}
	
	this.showProgressBar = function(){
		// Fade in progressBar + loading animation
		if(!Game.loader._obj) {
			Game.loader._createProgressBar();
		}else{
			createjs.Tween.get(Game.loader._obj).to({alpha:1},200);
		}
	}
	
	this.hideProgressBar = function(){
		// fade out progressbar + loading animation
		if(!!Game.loader._obj) {
			createjs.Tween.get(Game.loader._obj).to({alpha:0},200).call(Game.helper.removeTweenObject);
		}
	}
	
	this._loadBeatmaps = function(){
	
		Game.loader.queue.removeEventListener("complete", Game.loader._loadBeatmaps);
	
		var manifest = [];
		
		for(var i = 0; i < _data.length; i++){
			for(var j = 0; j < _data[i].beatmaps.length; j++){
				var songData = _data[i];
				var BeatmapData = _data[i].beatmaps[j]; 
				var id = songData.id + j;
				manifest.push({id: "bm_" + id, src: BeatmapData.dir, type: "text"});
			}
		}
	
		Game.loader.queue.loadManifest(manifest);
		
		Game.loader.queue.addEventListener("complete", Game.loader._onBeatmapsLoaded);
	}
	
	this._onBeatmapsLoaded = function(){
		
		Game.loader.queue.removeEventListener("complete", Game.loader._onBeatmapsLoaded);
	
		for(var k = 0; k < _data.length; k++){
			for(var z = 0; z < _data[k].beatmaps.length; z++){
				
				var id = _data[k].id + z;
				
				var result = Game.loader.queue.getResult("bm_" + id);
				if(!!result){
					
					var bm = new Beatmap();
					bm.parseFromTxt(result);
					bm.data = _data[k];
					bm.id = id;
				
					Game.beatmaps.push(bm);
				}
			}
		}
		
		// When Beatmaps are loaded, go to SongListMenu
		Game.loader._onCoreElementsLoaded();
	}
	
	// Core Elements
	
	this._loadCoreElements = function(){

		this.queue.loadManifest([
			// Background ( welcome )
			{id: "background", src: "game/assets/img/welcome.jpg"},

			// Le music
			//{id: "theme-song", src: "game/assets/sounds/nekodex - welcome to christmas!.mp3"},
			{id: "theme-song", src: "game/assets/sounds/Nekodex - Welcome to osu! (2).mp3"},
			
			{id: "volume-bg", src: "game/assets/img/volume-bg.png"},
			
			// Background ( menu )
			{id: "background_main", src: "game/assets/img/bg.jpg"},
			
			//SongMenuItem
			{id: "menu-button-background", src: "game/assets/img/menu-button-background.png"},
			
			// Cursor
			{id: "cursor-mid", src: "game/assets/img/cursormiddle.png"},
			{id: "cursor", src: "game/assets/img/cursor.png"},
			{id: "star2", src: "game/assets/img/star2.png"}
		]);
		
		Game.loader.queue.addEventListener("fileload", Game.loader._onCoreElementLoaded);
		Game.loader.queue.addEventListener("complete", Game.loader._loadBeatmaps);
	}
	
	this._onCoreElementLoaded = function(){
		
		// Only load the welcome bg
		Game.loader.queue.removeEventListener("fileload", Game.loader._onCoreElementLoaded);
		
		Game.UI.setBackground();
	}
	
	this._onCoreElementsLoaded = function(){
		
		// Set Background
		Game.UI.setBackground(true);
		
		// Create Cursor
		Game._cursor = new Cursor();
		Game._cursor.init();
		
		Game.mediaplayer.createVolumeControl();
		
		Game._stage.sortChildren(Game.helper.sortByZ);
		
		// wait half a second for smother transition
		setTimeout(function(){
			Game.UI.load('songList');}, 500);
	}
	
	// Core Menu Elements
	
	this._loadCoreMenuElements = function(){
	}
	
	this._onCoreMenuElementsLoaded = function (){
	}
	
	// Core Game Elements
	this._loadCoreGameElements = function(){
		
		Game.loader._createProgressBar();
		
		Game.loader.queue.loadManifest([
		
			// Other
			{id: "applause", src: "game/assets/sounds/applause.mp3"},
		
			// HitObject Sounds
			{id: "hit-normal", src: "game/assets/sounds/normal-hitnormal.mp3"},
			{id: "hit-hitfinish", src: "game/assets/sounds/normal-hitfinish.mp3"},
			{id: "hit-hitwhistle", src: "game/assets/sounds/normal-hitwhistle.mp3"},
			{id: "hit-sliderslide", src: "game/assets/sounds/normal-sliderslide.mp3"},
			{id: "hit-hitclap", src: "game/assets/sounds/normal-hitclap.mp3"},
			{id: "combobreak", src: "game/assets/sounds/combobreak.mp3"},
		
			// UI Items
			{id: "scorebar-bg", src: "game/assets/img/scorebar-bg.png"},
			{id: "score-bg", src: "game/assets/img/scorebar-bg.png"},
			{id: "score-mark", src: "game/assets/img/scorebar-marker.png"},
			
			// HitObject Images
			{id: "hitCircle", src: "game/assets/img/hitcircleoverlay.png"},
			{id: "sliderball", src: "game/assets/img/sliderb.png"},
			{id: "approachCircle", src: "game/assets/img/approachcircle.png"},
			{id: "hit-300", src: "game/assets/img/hit300.png"},
			{id: "hit-100", src: "game/assets/img/hit100.png"},
			{id: "hit-50", src: "game/assets/img/hit50.png"},
			{id: "hit-0", src: "game/assets/img/hit0.png"},
			{id: "spin-top", src: "game/assets/img/spinner-bottom.png"},
			{id: "spin-bot", src: "game/assets/img/spinner-top.png"},
			{id: "spin-ac", src: "game/assets/img/spinner-approachcircle.png"},
			{id: "spin-mid", src: "game/assets/img/spinner-middle2.png"},
			{id: "spin-bord", src: "game/assets/img/spinner-middle.png"},
			{id: "spin-rpm", src: "game/assets/img/spinner-rpm.png"},
			{id: "spin-txtSpin", src: "game/assets/img/spinner-spin.png"},
			{id: "spin-txtClear", src: "game/assets/img/spinner-clear.png"},
			
			// Menu Buttons
			{id: "btn-continue", src: "game/assets/img/pause-continue.png"},
			{id: "btn-retry", src: "game/assets/img/pause-retry.png"},
			{id: "btn-back-to-menu", src: "game/assets/img/pause-back.png"},
			
			{id: "btn-back", src: "game/assets/img/menu-back.png"},
			
			// Ranking
			{id: "rank-bg", src: "game/assets/img/ranking-panel.png"},
			{id: "rank-ss", src: "game/assets/img/ranking-X.png"},
			{id: "rank-s", src: "game/assets/img/ranking-S.png"},
			{id: "rank-a", src: "game/assets/img/ranking-A.png"},
			{id: "rank-b", src: "game/assets/img/ranking-B.png"},
			{id: "rank-c", src: "game/assets/img/ranking-C.png"},
			{id: "rank-d", src: "game/assets/img/ranking-D.png"},
		]);

		Game.loader.queue.addEventListener("complete", Game.loader._onCoreGameElementsLoaded);
	}
	
	this._onCoreGameElementsLoaded = function(){
		Game.loader.queue.removeEventListener("complete", Game.loader._onCoreGameElementsLoaded);
	
		Game.initializeSong();
	}
	
	this._loadPreview = function(obj){
	
		var bgpath = obj.data.dir + obj.background;
		var previewpath = obj.data.dir + "[preview]" + obj.general.AudioFilename;
	
		Game.loader.queue.loadManifest([
			{id: "background", src: bgpath, type: "image"},
			{id: "preview", src: previewpath}
		]);
		
		Game.loader.queue.addEventListener("complete", Game.loader._onPreviewLoaded);
	}
	
	this._onPreviewLoaded = function(){
		
		Game.loader.queue.removeEventListener("complete", Game.loader._onPreviewLoaded);
	
		Game.UI.setBackground();
		Game.mediaplayer.play("preview");
	}
	
	// Specific Song Elements
	
	this._loadSongElements = function(obj){
		// TODO :: currently only loads the song, but should also load skinned elements in the future!
		Game.loader._createProgressBar();
	
		Game.loader.queue.loadManifest([
			{id: "song", src: obj.data.dir + obj.general.AudioFilename},
		]);
		
		//Game.mediaplayer.currentSongBeatmap = obj;
		//Game.mediaplayer.currentSongBeatmap = obj;
	
		Game.loader.queue.addEventListener("complete", Game.loader._onSongElementsLoaded);
	}
	
	this._onSongElementsLoaded = function(){
		
		Game.loader.queue.removeEventListener("complete", Game.loader._onSongElementsLoaded);
		
		// load game UI ( sets game in motion )
		Game.UI.load('game');
	}
}