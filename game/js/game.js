var Game = new MainController();

function MainController(){
	// Main Stage
	this._stage;
	
	// Pauze Stage ( is needed because main-stage will be frozen -- so pauze menu stays working :D )
	this._pauseStage;
	
	// Game Private Variables ( no user input or change during playtime )
	this._cursor;
	this._time = 0;

	this._isLoading = false;
	this._isPaused = false;
	
	this.helper = new Helper();
	this.UI = new UIController();
	this.mediaplayer = new MediaPlayer();
	this.loader = new Loader();
	
	// Game Accessible Variables
	this.scores;
	this.player;
	this.settings;
	
	// Ingame variables like ( combo, life, score, hitpercentage )
	this.vars = {};
	
	this._beatmap;
	this.beatmaps = [];
	
	// Init
	this.init = function(){
		// Define Stage
		this._stage = new createjs.Stage("game_canvas");
		this._stage.enableMouseOver(15);					// TODO :: check if 15 fps for hover is enough?
		
		this._pauseStage = new createjs.Stage("game_canvas");
		this._pauseStage.enableMouseOver(15);		
		
		// Initialize child members
		this.UI.init();
		this.loader.init();
		this.mediaplayer.init();
		
		// Set FPS to 60 ( max 60 ) 
		createjs.Ticker.setFPS(60); 						// TODO :: check if 60 is optimal, and if FPS can be made higher if possible
		//createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED; 
		
		// Set Tick to listen to UpdateMenu
		createjs.Ticker.addEventListener("tick", update);
		
		// Register Events
		$("#game_canvas").on('mousewheel', OnMouseScroll);
		$("#game_canvas").attr('tabindex', 0);
		
		// Start Loading first elements
		this.loader._loadCoreElements();
	}
	
	this.initializeGame = function(obj){
		Game.loader._loadCoreGameElements();
	}
	
	this.initializeSong = function(){
		Game.loader._loadSongElements(Game._beatmap);
	}
	
	this.startGame = function(){
		
		// START GAME
		Game.mediaplayer._songStartTime = createjs.Ticker.getEventTime();
		
		// Start Song :D
		setTimeout(function(){
				
				Game.mediaplayer.play("song");
				Game.mediaplayer.currentSong.addEventListener("complete", Game.songFinished);
			
			}, 0//parseInt(Game._beatmap.general.AudioLeadIn)
		);
		
		Game._stage.enableMouseOver(0);
		
		// STARTED THE GAME?
	}
	
	// Game Pauze
	this.pause = function(){
		if(!!Game.mediaplayer && !!Game.mediaplayer.currentSong){
			Game.mediaplayer.pause();
		}
		
		createjs.Ticker.setPaused(true);
		
		Game._isPaused = true;
		
		Game.UI._onGamePause();
	}
	
	this.restart = function(){
	
		Game.UI.game.onLeave();
		
		Game.mediaplayer.stop();
	
		// Re-initialize game
		Game.initializeGame(Game._current);
	}
	
	this.resume = function(){
		if(Game.mediaplayer != null && Game.mediaplayer.currentSong != null){
			Game.mediaplayer.resume();		
		}
		
		createjs.Ticker.setPaused(false);
		
		Game._isPaused = false;
	}
	
	this.songFinished = function(){
		console.log('END OF SONG?!');
	}
	
	function OnMouseScroll(event, delta){
	
		if(Game._scrollForVolume){
			var volumeChange = event.originalEvent.deltaY / 1000;
			var currVolume = createjs.Sound.getVolume();
			createjs.Sound.setVolume(currVolume - volumeChange);
			drawVolume();
		}
	}
}

function update(event){
	
	if(!!event)
		Game._time = event.time;
	
	if(!!Game._cursor)
		Game._cursor.update();
	
	if(!!Game.UI._current)
		Game.UI._update();
		
	if(Game.UI.game == Game.UI._current){
		Game.UI.game._obj._txtFPS.text = createjs.Ticker.getMeasuredFPS().toFixed(2) + 'FPS';
	}
	
	// Update the stage in the end
	Game._stage.update();
}