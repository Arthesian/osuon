// JavaScript Document


$(document).ready(function() {
		$(".fancybox").fancybox({
		'hideOnContentClick': true,
		'openEffect': 'elastic', // elastic
		'closeEffect': 'elastic',
		'cyclic': true,
		closeBtn: false,
		scrolling: 'hidden',
		helpers: {
            overlay: {
              locked: true 
            }
        },
		'width': '820',
		'height': '90%',
		'speedIn': 500,
		'speedOut': 500,
		'titlePosition': 'over',
		'type': 'iframe'
		});
	});
