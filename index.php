<?php

session_start();

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

// Requires
require_once('includes\funclib.inc.php');
require_once('lib\smarty\Smarty.class.php');

// Includes
include('controllers\c_main.class.php');
include('controllers\c_indexer.class.php');

getCorrectPage();

// Create Smarty Object
$smarty = new Smarty();

// Assign variables
$smarty->assign('base_url', 'http://' . $_SERVER['SERVER_NAME']);
$smarty->assign('site_title',"OSU-ON! - The Online OSU HTML5 Experience");

$fileList = $c_indexer->getFileList();

$song_id;

$max = count($fileList) - 1;

if(isset($_GET['id'])){
	$id = $_GET['id'];
	
	if($id > $max){
		$id = rand(0, $max);
	}
	$song_id = $id;
}else{
	$song_id =rand(0, $max);
}

$smarty->assign('song_id', $song_id);
$smarty->assign('song_data', json_encode($fileList));

// Get Page Content
$content = $c_main->getContent(getArgs());

// Assign content to indexpage
$smarty->assign('content', $content);

// Display Page // if it fails :: display error
try{
	$smarty->display('templates/base.tpl');
}catch(Exception $e){
	echo $e;
}
?>