<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

<title>OSU - Online</title>

<!-- SCRIPTS -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<!-- OWN STYLESHEET -->
<link rel="stylesheet" type="text/css" href="../style/style.css"/>
</head>

<body>

<div id="wrapper">
	<div id="header">
    	<div id="logo"></div>
        <div id="slogan"></div>
        
        <div id="btn_upload"></div>
    </div>
    <div id="main_content">
    
    	{$content}
    
    </div>
    <div id="footer">
    	
    </div>
</div>


</body>
</html>
