<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.11.8/TweenMax.min.js"></script>
<script type="text/javascript" src="../game_webgl/js/createjs.js"></script>
<script type="text/javascript" src="../game_webgl/js/spriteContainer.js"></script>
<script type="text/javascript" src="../game_webgl/js/spriteStage.js"></script>
<script type="text/javascript" src="../game_webgl/js/functions.js"></script>

<script type="text/javascript" src="../game_webgl/js/beatmap.js"></script>
<script type="text/javascript" src="../game_webgl/js/hitobject.js"></script>
<script type="text/javascript" src="../game_webgl/js/osu_menu.js"></script>
<script type="text/javascript" src="../game_webgl/js/osu_game.js"></script>



<script>
var _data = {$song_data};
</script>

<script>


var _songDir;

var _stage, _stage_bg, _stage_cursor;
var _settings = new Object();
var _loadQueue;
var _progressBar, _progressBarText;
var _beatmap = new Object();
var _UI;
var _songProgress;

function init(){
	// Define Stage
	_stage = new createjs.Stage("game_canvas");
	_stage_bg = new createjs.Stage("bg_canvas");
	_stage_cursor = new createjs.Stage("cursor_canvas");
	
	_stage.enableMouseOver();
	
	// Define LoadQueue
	_loadQueue = new createjs.LoadQueue(false);
	
	createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin, createjs.FlashPlugin]);
	createjs.Sound.alternateExtensions = ["ogg"];
	
	_loadQueue.installPlugin(createjs.Sound, createjs.WebAudioPlugin, createjs.HTMLAudioPlugin);
	
	// Set FPS to 1000 ( max 200 )
	createjs.Ticker.setFPS(120); // MAX POWER
	
	initialLoading();
}
{literal}
function initialLoading(){
	
	_loadQueue.loadManifest([
		// UI Items
		{id: "background", src: "game/assets/img/bg.png"}, // first load background
		
		// Cursor
		{id: "cursor-mid", src: "game/assets/img/cursormiddle.png"},
		{id: "cursor", src: "game/assets/img/cursor.png"},
		
		// Other
		{id: "menuhit", src: "game/assets/sounds/menuhit.mp3"},
	]);
	
	//_loadQueue.addEventListener("progress", onQueueLoading);			// Show Progress
	_loadQueue.addEventListener("complete", initialLoadingComplete);
	_loadQueue.addEventListener("fileload", onQueuefileLoaded);		// Load files directly when completed ( background/ cursor )
}
{/literal}
function initialLoadingComplete(){

	createCursor();
	
	loadMenuScene();
	
	// Set Tick to listen to UpdateMenu
	createjs.Ticker.addEventListener("tick", updateMenu);
	
	_stage.sortChildren(sortByZ);
}


function loadMenuScene(){
	
	setLoadedBackground();
	
	createMenu(_data);
}

function updateMenu(){
	
	updateCursor();
	
	_stage.update();
}
{literal}
// Original INIT();
function init2(beatmap){
	
	$("#game_topbar").animate({top: -50}, 400).fadeOut(500);

	_loadQueue.loadManifest([
		// UI Items
		{id: "scorebar-bg", src: "game/assets/img/scorebar-bg.png"},
		{id: "volume-bg", src: "game/assets/img/volume-bg.png"},
		{id: "score-bg", src: "game/assets/img/scorebar-bg.png"},
		{id: "score-mark", src: "game/assets/img/scorebar-marker.png"},

		// Selected Song Items
		{id: "beatmap", src: beatmap.dir, type: "text"},
		{id: "song", src: beatmap.song},
		
		// HitObject Images
		{id: "hitCircle", src: "game/assets/img/hitcircleoverlay.png"},
		{id: "approachCircle", src: "game/assets/img/approachcircle.png"},
		{id: "hit-300", src: "game/assets/img/hit300.png"},
		{id: "hit-100", src: "game/assets/img/hit100.png"},
		{id: "hit-50", src: "game/assets/img/hit50.png"},
		{id: "hit-0", src: "game/assets/img/hit0.png"},
		{id: "spin-top", src: "game/assets/img/spinner-bottom.png"},
		{id: "spin-bot", src: "game/assets/img/spinner-top.png"},
		{id: "spin-ac", src: "game/assets/img/spinner-approachcircle.png"},
		{id: "spin-mid", src: "game/assets/img/spinner-middle2.png"},
		{id: "spin-bord", src: "game/assets/img/spinner-middle.png"},
		{id: "spin-rpm", src: "game/assets/img/spinner-rpm.png"},
		{id: "spin-txtSpin", src: "game/assets/img/spinner-spin.png"},
		{id: "spin-txtClear", src: "game/assets/img/spinner-clear.png"},
		
		// HitObject Sounds
		{id: "hit-normal", src: "game/assets/sounds/normal-hitnormal.mp3"},
		{id: "hit-hitfinish", src: "game/assets/sounds/normal-hitfinish.mp3"},
		{id: "hit-hitwhistle", src: "game/assets/sounds/normal-hitwhistle.mp3"},
		{id: "hit-sliderslide", src: "game/assets/sounds/normal-sliderslide.mp3"},
		{id: "hit-hitclap", src: "game/assets/sounds/normal-hitclap.mp3"},
		{id: "combobreak", src: "game/assets/sounds/combobreak.mp3"},
		
		// Other
		{id: "applause", src: "game/assets/sounds/applause.mp3"},
		
		// Ranking
		{id: "rank-bg", src: "game/assets/img/ranking-panel.png"},
		{id: "rank-ss", src: "game/assets/img/ranking-X.png"},
		{id: "rank-s", src: "game/assets/img/ranking-S.png"},
		{id: "rank-a", src: "game/assets/img/ranking-A.png"},
		{id: "rank-b", src: "game/assets/img/ranking-B.png"},
		{id: "rank-c", src: "game/assets/img/ranking-C.png"},
		{id: "rank-d", src: "game/assets/img/ranking-D.png"},
	]);
	
	_loadQueue.addEventListener("progress", onQueueLoading);
	_loadQueue.addEventListener("complete", handleComplete);
	_loadQueue.addEventListener("fileload", onQueuefileLoaded);

	_progressBar = new createjs.Shape();
	_progressBarText = new createjs.Text("0%", "20px Arial", "#FAA");
	_stage.addChild(_progressBar);
	_stage.addChild(_progressBarText);
}

function onQueueLoading(event){
	
	_progressBar.graphics.clear();
	_progressBar.zIndex = 10;
	_progressBarText.zIndex = 11;
	
	var text = (event.progress*100).toFixed(2);
	
	_progressBarText.text = text + '%';
	
	// Draw the progress bar
	var width = 316, height = 20;
	var loaded = width*(event.progress / event.total);
	
	var x = (_stage.canvas.width * 0.5) - ( width * 0.5);
	var y = (_stage.canvas.height * 0.6) - ( height * 0.5);
	
	_progressBar.graphics.beginStroke("#FAA").drawRect( x, y, width, height);
    _progressBar.graphics.beginFill("#FCC").drawRect( x, y, loaded, height);
	
	_progressBarText.x = (_stage.canvas.width * 0.5) - 35;
	_progressBarText.y = y + 30;

	update();
}

function sortByZ(a,b) {
    if (a.zIndex < b.zIndex) return -1;
    if (a.zIndex > b.zIndex) return 1;
    return 0;
}

function onQueuefileLoaded(event){
	var obj = event.item;
	
	if(obj.id == 'background'){
		
		setLoadedBackground();
		
		// Change progress color accordingly
		//_progressBarText.color = "#FAA";
	}
	
	if(obj.id == "beatmap"){
		parseBeatmap(_loadQueue.getResult("beatmap"));
	}
}

function osuXYtoGameXY(x,y){
	var GameX = 0.5 * (_stage.canvas.width - 700) + (x / 512) * 700;
	var GameY = 100 + (y / 384) * (_stage.canvas.height - 200);
	
	return {x: GameX, y: GameY};
}

var comboColors = ['255,50,50', '0,200,0', '50,50,255', '230,200,50']; // red, green, blue, yellow-ish

function createCursor(){
	_cursor = new createjs.Container();
	_cursor.width = 76;
	_cursor.height = 76;
	_cursor.name = "cursor";
	
	_cursor.x = -76;
	
	var inner = new createjs.Bitmap(_loadQueue.getResult("cursor-mid"));
	var outer = new createjs.Bitmap(_loadQueue.getResult("cursor"));
		
	inner.regX = inner.regY = inner.image.width * 0.5;
	outer.regX = outer.regY = outer.image.width * 0.5;
		
	createjs.Tween.get(outer, {loop:true}).to({rotation: 360}, 4000);
		
	_cursor.addChild(inner, outer);
	_cursor.zIndex = 9999;
	
	var x = _stage.getChildByName('cursor');
	
	if(x){
		_stage.removeChild(x);
	}
	
	_stage.addChild(_cursor);

	_cursorLoaded = true;
}

var _songStartTime;

function handleComplete(event){
	
	_songStartTime = createjs.Ticker.getEventTime();
	
	$("#game_canvas").on('mousewheel', OnMouseScroll);
	
	_stage.clear();
	
	// Remove loading bar from _stage
	_stage.removeChild(_progressBar);
	_stage.removeChild(_progressBarText);
	
	_beatmap.leadInTime = parseInt(_beatmap['General']['AudioLeadIn']);
	_beatmap.hitObjSize = 50 + (10 - parseInt(_beatmap['Difficulty']['CircleSize'])) / 9 * 100;
	_beatmap.difficulty = 25 + (10 - parseInt(_beatmap['Difficulty']['OverallDifficulty'])) / 9 * 125;
	_beatmap.approachRate = 300 + (10 - parseInt((_beatmap['Difficulty']['ApproachRate'])?_beatmap['Difficulty']['ApproachRate']: 7)) / 9 * 1200;
	
	_stage.currentComboColorIndex = 0; // start
	
	_UI = new createjs.Container();
	_UI.x = _UI.y = 0;
	_UI.width = _stage.canvas.width;
	_UI.height = _stage.canvas.height;
	_UI.zIndex = 50;
	
	_UI._txtCombo = new createjs.Text("0X", "64px Verdana", "#FFF");
	
	_UI._txtCombo.x =  10;
	_UI._txtCombo.y = _stage.canvas.height - _UI._txtCombo.getMeasuredHeight() - 15;
	
	_UI._txtPercent = new createjs.Text("100.00%", "48px Verdana", "#FFF");
	
	_UI._txtPercent.textAlign = 'right';
	
	_UI._txtPercent.x = _stage.canvas.width - 10;
	_UI._txtPercent.y = _stage.canvas.height - _UI._txtPercent.getMeasuredHeight() - 15;
	
	_UI._txtFPS = new createjs.Text("0.00FPS", "20px Verdana", "#FF0");
	
	_UI._txtFPS.textAlign = 'right';
	
	_UI._txtFPS.x = _stage.canvas.width - 10;
	_UI._txtFPS.y = _stage.canvas.height - _UI._txtPercent.getMeasuredHeight() - _UI._txtFPS.getMeasuredHeight() - 15;
	
	_UI._txtScore = new createjs.Text("0", "48px Verdana", "#FFF");
	
	_UI._txtScore.textAlign = 'right';
	
	_UI._txtScore.x = _stage.canvas.width - 10;
	_UI._txtScore.y = 5;
	
	var str = _beatmap['Metadata']['Artist'] + ' - ' + _beatmap['Metadata']['Title'] + '\n[' +  _beatmap['Metadata']['Version'] + ']';
	_UI._txtSongTitle = new createjs.Text(str, "18px Verdana", "#FFF");
	
	_UI._txtSongTitle.x = 10;
	_UI._txtSongTitle.y = 40;
	
	_UI._progress = new createjs.Container();
	
	_UI._progress.x = _stage.canvas.width - 35;
	_UI._progress.y = 85;
	_UI._progress.height = _UI._progress.width = 2000;
	
	var p_bord = new createjs.Shape();
	p_bord.graphics.ss(2).s("rgba(255,255,255,0.7)").a(0,0,20,0,Math.PI*2);
	
	_songProgress = new createjs.Shape();
	_songProgress.graphics.f("rgba(255,255,255,0.5)").a(0,0,20,0,Math.PI*2);
	
	_UI._progress.addChild(_songProgress, p_bord);
	
	_UI.volume = new createjs.Bitmap(_loadQueue.getResult("volume-bg"));
	_UI.volume.x = _stage.canvas.width - _UI.volume.image.width;
	_UI.volume.y = _stage.canvas.height * 0.5 - _UI.volume.image.height * 0.5;
	
	_UI.scoreBar = new createjs.Bitmap(_loadQueue.getResult("score-bg"));
	_UI.scoreBar.x = _UI.scoreBar.y = 0;

	// GAME SQUARE:: ( TEST );
	var gameStage = new createjs.Shape();
	gameStage.zIndex = 1;
	
	var minXY = osuXYtoGameXY(0,0);
	var maxXY = osuXYtoGameXY(512, 384);
	
	//gameStage.graphics.f("rgba(0,0,255,0.5)").drawRect(minXY.x, minXY.y, maxXY.x - minXY.x, maxXY.y - minXY.y);
	gameStage.graphics.rf(["rgba(0,0,0,0)","rgba(0,0,0,0.7)"], [0.3,1], 640, 360, 0, 640,360,640).drawRect(0,0,_stage.canvas.width,_stage.canvas.height);
	
	_stage.addChildAt(gameStage, 1);

	_UI.addChild(_UI.volume, _UI._txtSongTitle, _UI._txtPercent, _UI._txtScore, _UI._txtCombo, _UI.scoreBar, _UI._txtFPS, _UI._progress);
	_stage.addChild(_UI);
	
	// Set global volume
	createjs.Sound.setVolume(0.5);
	
	drawVolume();
	
	// Start Song :D
	setTimeout(function(){
			
			_currSong = createjs.Sound.play("song");
			_currSong.addEventListener("complete", songFinished);
		
		}, parseInt(_beatmap['General']['AudioLeadIn'])
	);
	
	//createjs.Ticker.setFPS(1000); // MAX POWER
	createjs.Ticker.removeEventListener('tick',updateMenu);
	createjs.Ticker.addEventListener("tick", update);
}

function updateScore(){
	_score = _score + (_currComboSchoresPlayed.perfect * 300 + _currComboSchoresPlayed.good * 100 + _currComboSchoresPlayed.bad * 50) * (_currCombo);
}

function calculateScore(){
	_currScore = _score + (_currComboSchoresPlayed.perfect * 300 + _currComboSchoresPlayed.good * 100 + _currComboSchoresPlayed.bad * 50) * (_currCombo);
	
	_UI._txtScore.text = _currScore;
}

function calculateHitPercentage(){
	
	var total = _hitScoresPlayed.perfect + _hitScoresPlayed.good + _hitScoresPlayed.bad + _hitScoresPlayed.miss;
	
	var maxScore = total * 300;
	
	var score = _hitScoresPlayed.perfect * 300 + _hitScoresPlayed.good * 100 + _hitScoresPlayed.bad * 50;
	
	var perc = score / maxScore * 100;
	
	_currHitPercentage = parseFloat(perc.toFixed(2));
	_UI._txtPercent.text = perc.toFixed(2) + '%';
}

function removeTweenObject(tween){
	_stage.removeChild(tween._target);
}

function removeObject(event){
	_stage.removeChild(event.target);
}

var _currLifeTotal = 1000; // max == 1000;
var _lifeDrainPaused = false;

var _hitScoresPlayed = { perfect: 0, good: 0, bad: 0, miss: 0 };
var _currComboSchoresPlayed = { perfect: 0, good: 0, bad: 0, miss: 0 };

var _combosPlayed = [];
var _hitObjectsPlayed = 0;

var _currHitPercentage = 100;
var _score = 0;
var _currScore = 0;
var _currCombo = 0;
var _spawnCount = 0;
var _currStageTime;

var _currSong;

function songFinished(event){
	console.log("game's over");
	
	_lifeDrainPaused = true;
	
	// create endScreen
	var endScreen = new createjs.Container();
	
	var scoreBG = new createjs.Bitmap(_loadQueue.getResult("rank-bg"));
	
	var scoreRank;
	var perc = parseInt(_currHitPercentage.toFixed(0));
	if(_currHitPercentage ==  100){
		scoreRank = new createjs.Bitmap(_loadQueue.getResult("rank-ss"));
	}else if(_currHitPercentage > 93 && _hitScoresPlayed.miss == 0){
		scoreRank = new createjs.Bitmap(_loadQueue.getResult("rank-s"));
	}else if(_currHitPercentage > 90 && _hitScoresPlayed.miss == 0 || _currHitPercentage > 93){
		scoreRank = new createjs.Bitmap(_loadQueue.getResult("rank-a"));
	}else if(_currHitPercentage > 80){
		scoreRank = new createjs.Bitmap(_loadQueue.getResult("rank-b"));
	}else if(_currHitPercentage > 60){
		scoreRank = new createjs.Bitmap(_loadQueue.getResult("rank-c"));
	}else{
		scoreRank = new createjs.Bitmap(_loadQueue.getResult("rank-d"));
	}
	
	scoreBG.x = _stage.canvas.width * 0.5 - scoreBG.image.width * 0.80;
	scoreBG.y = 100;
	
	scoreRank.x = _stage.canvas.width * 0.5 + scoreRank.image.width * 0.45;
	scoreRank.y = 190;
	
	endScreen.addChild(scoreBG, scoreRank);
	endScreen.zIndex = 100;
	_stage.addChild(endScreen);
	
	_stage.sortChildren(sortByZ);
}

function updateLife(value){
	
	if(_lifeDrainPaused) return;
	
	_currLifeTotal += value;
	
	if(_currLifeTotal > 1000){
		_currLifeTotal = 1000;
	}
	
	if(_currLifeTotal <= 0 ){
		// END GAME :(
		
		console.log("Actually....you died :(");
		
		_currLifeTotal = 1000; // reset because I'm a good person
	}
	
	var life = new createjs.Shape();
	life.name = 'lifeBarFill';
	life.zIndex = 55;
	
	var pt_life = (_currLifeTotal / 1000) * 636
	
	life.graphics.ss(9, "round").s("white").mt(17,18).lt( 17 + pt_life, 18);
	
	var oldLife = _stage.getChildByName("lifeBarFill");
	if(oldLife){
		_stage.removeChild(oldLife);
	}
	
	_stage.addChild(life);
}

var _prevHitObj;
var _currHitObj;
var _nextHitObj;

function update(event){
	
	if(!event) {_stage.update(); return;}
	
	_currStageTime = event.time;
	
	
	if(!!_beatmap.hitobjects){
		
		if(!_prevHitObj){
			// First Run
			_prevHitObj = 'filled';
			_currHitObj = _beatmap.hitobjects[0];
			_nextHitObj = _beatmap.hitobjects[1];
		}
		
		if(!!event && _currHitObj && (event.time - _songStartTime + 2050) >= _currHitObj.time + _beatmap.leadInTime){
			
			//_currHitObj.isPlayed = true;
			createHitObject(_currHitObj);
			
			_prevHitObj = _currHitObj;
			_currHitObj = _nextHitObj;
			_nextHitObj = (_nextHitObj.index < _beatmap.hitobjects.length - 1) ? _beatmap.hitobjects[_nextHitObj.index+1] : null;
		}
	}
	
	updateLife(- 0.5 - (_beatmap["Difficulty"]["HPDrainRate"] / 9));
	
	updateCursor();
	
	updateProgress();
	
	_UI._txtFPS.text = createjs.Ticker.getMeasuredFPS().toFixed(2) + 'FPS';
	
	_stage.update();
}

_cursorLoaded = false;
_cursor = {};

function updateCursor(){
	
	_cursor.x = _stage.mouseX;
	_cursor.y = _stage.mouseY;
}

function updateProgress(){
	
	if(!_currSong) return;
	
	_songProgress.graphics.clear();
	
	var p = _currSong.getPosition() / _currSong.getDuration();
	
	_songProgress.graphics.f('rgba(255,255,255,0.5)');//.a(0,0,20,0,(1-p) * Math.PI * 2);
	drawSegment(_songProgress, 0, 0, 20,(p*360) - 90, 270)
}

function drawSegment(target, x, y, r, aStart, aEnd, step) {
	
		if(!step) step = 10;
		// More efficient to work in radians
		var degreesPerRadian = Math.PI / 180;
		aStart *= degreesPerRadian;
		aEnd *= degreesPerRadian;
		step *= degreesPerRadian;
		
		// Draw the segment
		target.graphics.moveTo(x, y);
		for (var theta = aStart; theta < aEnd; theta += Math.min(step, aEnd - theta)) {
			target.graphics.lineTo(x + r * Math.cos(theta), y + r * Math.sin(theta));
		}
		target.graphics.lineTo(x + r * Math.cos(aEnd), y + r * Math.sin(aEnd));
		target.graphics.lineTo(x, y);
};

function checkClickedHitobject(){
	
	var hits = [];
	
	for(i = 1; i < _stage.children.length; i++){
		var obj =  _stage.children[i];
		if(!obj.children || !obj.hitObj) continue;
		
		var pt = obj.children[1].globalToLocal(_stage.mouseX, _stage.mouseY);
		
		if(obj.children[1].hitTest(pt.x, pt.y)){
			hits.push(obj); 
		}
		
	}
	hits.sort(sortByZ);
	
	hit = hits[hits.length - 1];
	
	if(!hit) return;
	
	hit.mouseEnabled = false;
	
	var e = {};
	e.target = {};
	e.target.parent = hit;
	clickedHitcircle(e);
	
}

function OnMouseScroll(event, delta){
	
	var volumeChange = event.originalEvent.deltaY / 1000;
	
	var currVolume = createjs.Sound.getVolume();
	
	createjs.Sound.setVolume(currVolume - volumeChange);
	
	drawVolume();
}

function drawVolume(){
	
	var volBarFill = new createjs.Shape();
	
	var pt_bot = {x: _stage.canvas.width - 7, y: _stage.canvas.height * 0.5 + 110};
	var pt_rtop = {x: _stage.canvas.width - 7, y: (_stage.canvas.height * 0.5 - 110) + 220 * (1 - createjs.Sound.getVolume())};
	var pt_ltop = {x: (_stage.canvas.width - 14) + 7 * (1 - createjs.Sound.getVolume()), y:(_stage.canvas.height * 0.5 - 110) + 220 * (1 - createjs.Sound.getVolume())};
	
	volBarFill.graphics.beginFill("white").mt(pt_bot.x,pt_bot.y).lt(pt_ltop.x,pt_ltop.y).lt(pt_rtop.x,pt_rtop.y).es();
	
	
	var volBar = new createjs.Container();
	volBar.addChild(volBarFill);
	volBar.name = 'volumeBar';
	volBar.zIndex = 55;
	
	var oldVolBar = _stage.getChildByName('volumeBar');
	if(oldVolBar)
		_stage.removeChild(oldVolBar);
	
	_stage.addChild(volBar);
}

function cursorClick(){
	createjs.Tween.get(_cursor).to({scaleX:1.2, scaleY:1.2}, 50).to({scaleX:1, scaleY:1}, 100);
}

$(document).ready(function(e) {
	var x = window.innerWidth;
	var y = window.innerHeight;
	resized(x, y);
	
    //init();
	
	$("#game_canvas").attr('tabindex', 0);
	$("#game_canvas").on('keypress', checkClickedHitobject);
	$("#game_canvas").on('keypress', cursorClick);
	$("#game_canvas").on('click', cursorClick);

});

function resized(x, y){
	
  	//$("#game_canvas").attr('height', y).attr('width', x);
}

$(window).resize(function(event) {
	var x = event.currentTarget.innerWidth;
	var y = event.currentTarget.innerHeight;
	
	resized(x, y);
});

</script>
{/literal}

<div id="game">
	
    <div id="game_topbar">
        <form>
            <input id="searchBox" type="text" placeholder="Search..." />
        </form>
        
        <div id="user_box">
        	<div id="txt_username"></div>
            <div id="txt_userplays"></div>
            <div id="txt_maps_availalbe"></div>
        </div>
    </div>

    <canvas id="bg_canvas" width="1280" height="720"></canvas>
    
    <canvas id="game_canvas" width="1280" height="720">
    
    </canvas>
    
    <canvas id="cursor_canvas" width="1280" height="720">
        <p>Canvas not supported!</p>
	</canvas>
    
     <div id="game_botbar">
    	<div id="user_scores">
        	<ul>
            	<li>SS:</li>
                <li>S:</li>
                <li>A:</li>
                <li>B:</li>
                <li>C:</li>
                <li>D:</li>
            </ul>
        </div>
    </div>
   
    
</div>