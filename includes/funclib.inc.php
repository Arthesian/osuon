<?php
	// nicely formatted print_r for easy debugging
	function p($a){
		if (array_key_exists('SHELL', $_SERVER)){
			printf("%s\n", print_r($a, true));
			return;
		}
		
		if (isset($_SERVER['HTTP_ACCEPT']) 
			&& preg_match('/json$/', $_SERVER['HTTP_ACCEPT']))
		{
			print json_encode($a);
			return;
		}
		
		if (is_array($a)
			|| is_object($a))
		{
			$s_output = print_r($a, true);
		} elseif(is_string($a)){
			$s_output = $a;
		} else {
			$s_output = var_export($a, true);
		}
		
		printf("<pre style=\"clear:both; font-size:11px; font-family: monospace;"
			. "background-color: #edf7ff; margin: 5px; color:black; padding: 5px; "
			. "border:1px solid #d3e0eb\" class=\"debug\">%s</pre>\n", 
				$s_output);
	}
	
	function getCorrectPage(){
		$s_old_url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		$url = explode('/', $_SERVER['REQUEST_URI']);
		
		if($url[count($url)-1] == ''){
			unset($url[count($url)-1]);
		}
		
		$s_querystring = '';
		if(('?'.$_SERVER['QUERY_STRING']) == $url[count($url)-1]){
			$s_querystring = '?'.$_SERVER['QUERY_STRING'];
			unset($url[count($url)-1]);
		}
		
		$new_url = implode('/', $url).'/';
		
		$s_new_url = "http://".$_SERVER['HTTP_HOST'].$new_url.$s_querystring;
		
		if($s_new_url != $s_old_url){
			header("Location:".$s_new_url);
			exit;
		}else{
			return true;
		}
	}
	
	function getArgs(){
		return explode('/', $_SERVER['REQUEST_URI']); 
	}
	
	function getAssetDir(){
		$s_asset_dir = "";
		for($i=0;$i < count(getArgs())-2; $i++){
			$s_asset_dir = '../' . $s_asset_dir;
		}
		return ($s_asset_dir == '') ? '' : $s_asset_dir;
	}
	
?>