<?php 

class MySQLDatabase{
	
	private $connection;
	private $magic_quotes_active;
	private $real_escape_string_exists;
	public $last_query;
	
	//Automatisch openen van de connectie
	function __construct(){
		$this->openConnection();
		$this->magic_quotes_active = get_magic_quotes_gpc();
		$this->real_escape_string_exists = function_exists("mysql_real_escape_string");
	}
	
	//Connectie maken met de database.
	public function openConnection(){
		
		$username = 'root';
		$password = 'mysqlserver';
		$database = 'acapp';
		
		$this->connection = mysql_connect('localhost', $username, $password, $database);
		if(!$this->connection){
			die("Data connection mislukt: ". mysql_error());
		}
		mysql_select_db($database,$this->connection) or die(mysql_error());
	}
	
	//Connectie met de database afsluiten.
	public function closeConnection(){
		if(isset($this->connection)){
			mysql_close($this->connection);
			unset($this->connection);
		}
	}
	
	//De query ophalen.
	public function query($sql){
		$this->last_query = $sql;
		$result = mysql_query($sql, $this->connection);
		$this->confirmQuery($result);
		return $result;
	}
	
	//De query controleren op vreemde tekens.
	public function escapeValue($value){
		if($this->real_escape_string_exists){
			if($this->magic_quotes_active){
				$value = stripslashes($value);
			}
			$value = mysql_real_escape_string($value);
		}else{
			if(!$this->magic_quotes_active){
				$value = addslashes($value);
			}
		}
		return $value;
	}
	
	//Ervoor zorgen dat de functie fetch array wordt uitgevoerd.
	public function fetchArray($result_set){
		return mysql_fetch_array($result_set);
	}
	
	//Ervoor zorgen dat de functie fetch assoc wordt uitgevoerd.
	public function fetchAssoc($result_set){
		return mysql_fetch_assoc($result_set);
	}
	
	//Ervoor zorgen dat de functie num rows wordt uitgevoerd.
	public function numRows($result_set){
		return mysql_num_rows($result_set);
	}
	
	//Kijken of de query correct is uitgevoerd.
	private function confirmQuery($result){
		if(!$result){
			$output = "Database query mislukt: ". mysql_error() ."<br /><br />";
			$output .= "Laatste SQl query: ". $this->last_query;
			die($output);
		}
	}
}

//Nieuwe instance aanmaken van database.
$database = new MySQLDatabase();
?>