<?php 

class FileIndexer{
	
	private static $directory = "game/assets/beatmaps/";
	
	public function getFileList(){
		$songs = array();
		
		$r_song	= scandir(self::$directory);	// Root dir array
		
		foreach($r_song as $dir){
			
			if($dir == '.' || $dir == '..') continue;
			
			$map = new stdClass();
			
			$map->dir = self::$directory . $dir . '/';
			$tmp = preg_split('/ /', $dir, 2);
			
			$map->id = $tmp[0];
			$map->name = substr(strstr($dir," "), 1);
			
			$tmp = preg_split('/ - /', $map->name);
			$map->artist = $tmp[0];
			$map->song = $tmp[1];
			$map->contents = scandir($map->dir);
			
			$map->beatmaps = self::getBeatmapsFromDir($map->dir);
			
			$songs[] = $map;
		}
		
		return $songs;
	}
	
	private function getBeatmapsFromDir($dir){
		$contents = scandir($dir);
		
		$beatmaps = array();
		
		foreach($contents as $file){
			if(preg_match('/\.osu/',$file)){
				
				preg_match('/\[.*]/', $file, $matches);
				
				$tmp = new stdClass();
				$tmp->difficulty = $matches[0];
				$tmp->dir = $dir . $file;
				$tmp->song = $dir . self::parseSongFromBeatmap($tmp->dir);
				$beatmaps[] = $tmp;
			}
		}
		
		return $beatmaps;
	}
	
	private function parseSongFromBeatmap($file){
		
		$raw = file_get_contents($file);
		
		$output = str_replace(array("\r\n", "\r"), "\n", $raw);
		$txt = explode("\n", $output);

		foreach($txt as $line){
		
			if(strpos($line, 'AudioFilename:') !== false){
				$tmp = preg_split('/:\s*/', $line);
				
				return $tmp[1];
			}
		}
	
	}
}

$c_indexer = new FileIndexer();

?>